package edu.di.unimi.it.compilers.project.warnings;

import edu.di.unimi.it.compilers.project.lexing.LanguageLexer;
import edu.di.unimi.it.compilers.project.parsing.LanguageParser;
import edu.di.unimi.it.compilers.project.parsing.ast.instructions.Program;
import edu.di.unimi.it.compilers.project.reporting.ErrorReporter;
import java_cup.runtime.ComplexSymbolFactory;
import org.junit.Test;

import java.io.StringReader;
import java.util.Optional;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.refEq;
import static org.mockito.Mockito.*;

public class UninitializedVariableDetectorTests {
    @Test
    public void testDetectsUninitializedVariable() throws Exception {
        Program program = parseFromText("output a\n").get();
        ErrorReporter errorReporter = mock(ErrorReporter.class);

        UninitializedVariableDetector uninitializedVariableDetector = new UninitializedVariableDetector(errorReporter);

        assertFalse(program.accept(uninitializedVariableDetector));
        verify(errorReporter).getWarningMessage(eq("uninitialized variable 'a'"),
                refEq(new ComplexSymbolFactory.Location("TestInput", 1, 8)));
    }

    @Test
    public void testDoesNotDetectInitializedVariable() throws Exception {
        Program program = parseFromText("a = 5\noutput a\n").get();
        ErrorReporter errorReporter = mock(ErrorReporter.class);

        UninitializedVariableDetector uninitializedVariableDetector = new UninitializedVariableDetector(errorReporter);

        assertTrue(program.accept(uninitializedVariableDetector));
        verifyNoMoreInteractions(errorReporter);
    }

    @Test
    public void testDetectsMaybeUninizializedVariable() throws Exception {
        Program program = parseFromText("output (input ? (a = 3) : 5)\noutput a\n").get();
        ErrorReporter errorReporter = mock(ErrorReporter.class);

        UninitializedVariableDetector uninitializedVariableDetector = new UninitializedVariableDetector(errorReporter);

        assertFalse(program.accept(uninitializedVariableDetector));
        verify(errorReporter).getWarningMessage(eq("uninitialized variable 'a'"),
                refEq(new ComplexSymbolFactory.Location("TestInput", 2, 8)));
        verifyNoMoreInteractions(errorReporter);
    }

    @Test
    public void testDoesNotDetectVariableInitializedInBothBranches() throws Exception {
        Program program = parseFromText("output (input ? (a = 3) : (a = 5))\noutput a\n").get();
        ErrorReporter errorReporter = mock(ErrorReporter.class);

        UninitializedVariableDetector uninitializedVariableDetector = new UninitializedVariableDetector(errorReporter);

        assertTrue(program.accept(uninitializedVariableDetector));
        verifyNoMoreInteractions(errorReporter);
    }

    private Optional<Program> parseFromText(String text) throws Exception {
        StringReader reader = new StringReader(text);
        ComplexSymbolFactory factory = new ComplexSymbolFactory();
        LanguageLexer lexer = new LanguageLexer(reader, factory, "TestInput");
        LanguageParser parser = new LanguageParser(lexer, factory);

        Program result = (Program) parser.parse().value;
        return parser.errorCount() > 0 ? Optional.empty() : Optional.of(result);
    }
}
