package edu.di.unimi.it.compilers.project.compilation.tac;

import edu.di.unimi.it.compilers.project.lexing.LanguageLexer;
import edu.di.unimi.it.compilers.project.parsing.LanguageParser;
import edu.di.unimi.it.compilers.project.parsing.ast.instructions.Program;
import edu.di.unimi.it.compilers.project.reporting.ErrorReporter;
import edu.di.unimi.it.compilers.project.representations.tac.ThreeAddressGenerator;
import edu.di.unimi.it.compilers.project.representations.tac.instructions.ThreeAddressProgram;
import edu.di.unimi.it.compilers.project.warnings.UninitializedVariableDetector;
import java_cup.runtime.ComplexSymbolFactory;
import lt.macchina.Codice;
import lt.macchina.Macchina;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.*;
import java.util.Optional;

import static org.junit.Assert.assertEquals;

public class TACToMachineCompilerTests {
    private PrintStream systemStdOut;
    private InputStream systemStdIn;
    private ByteArrayOutputStream testOutputStream;
    private PipedOutputStream testInputStream;

    @Before
    public void setUpFakeStreams() throws Exception {
        systemStdOut = System.out;
        systemStdIn = System.in;

        testInputStream = new PipedOutputStream();
        System.setIn(new PipedInputStream(testInputStream));

        testOutputStream = new ByteArrayOutputStream();
        System.setOut(new PrintStream(testOutputStream));
    }

    @After
    public void restoreStdInAndStdOut() {
        System.setOut(systemStdOut);
        System.setIn(systemStdIn);
    }

    @Test(timeout = 1000)
    public void testEvenProgram() throws Exception {
        Program even = parseFromFile("even.prog").get();
        File generatedCodeFile = getGeneratedCodeFile(even, "evenProg");
        testInputStream.write("10\n".getBytes());
        Macchina.main(generatedCodeFile.getPath());
        assertEquals("numero? pari\n", testOutputStream.toString());

        testOutputStream.reset();
        testInputStream.write("11\n".getBytes());
        Macchina.main(generatedCodeFile.getPath());
        assertEquals("numero? dispari\n", testOutputStream.toString());
    }

    @Test(timeout = 1000)
    public void testMCDProgram() throws Exception {
        Program mcd = parseFromFile("MCD.prog").get();
        File generatedCodeFile = getGeneratedCodeFile(mcd, "mcdProg");
        testInputStream.write("54\n36\n".getBytes());
        Macchina.main(generatedCodeFile.getPath());
        assertEquals("Primo numero? Secondo numero? mcd = 18\n", testOutputStream.toString());
    }

    @Test(timeout = 1000)
    public void testMinProgram() throws Exception {
        Program min = parseFromFile("min.prog").get();
        File generatedCodeFile = getGeneratedCodeFile(min, "minProg");
        testInputStream.write("54\n36\n".getBytes());
        Macchina.main(generatedCodeFile.getPath());
        assertEquals("primo numero? secondo numero? 36\n", testOutputStream.toString());
    }

    @Test(timeout = 1000)
    public void testMultProgram() throws Exception {
        Program mult = parseFromFile("mult.prog").get();
        File generatedCodeFile = getGeneratedCodeFile(mult, "multProg");
        testInputStream.write("3\n".getBytes());
        Macchina.main(generatedCodeFile.getPath());
        assertEquals("inserisci il massimo numero da considerare " +
                        "1 * 1 = 1\n" +
                        "1 * 2 = 2\n" +
                        "1 * 3 = 3\n" +
                        "2 * 1 = 2\n" +
                        "2 * 2 = 4\n" +
                        "2 * 3 = 6\n" +
                        "3 * 1 = 3\n" +
                        "3 * 2 = 6\n" +
                        "3 * 3 = 9\n",
                testOutputStream.toString());
    }

    @Test(timeout = 1000)
    public void testPrimesProgram() throws Exception {
        Program primes = parseFromFile("primes.prog").get();
        File generatedCodeFile = getGeneratedCodeFile(primes, "primesProg");
        testInputStream.write("20\n".getBytes());
        Macchina.main(generatedCodeFile.getPath());
        assertEquals("massimo? 2\n3\n5\n7\n11\n13\n17\n19\n", testOutputStream.toString());
    }

    private File getGeneratedCodeFile(Program program, String fileName) throws IOException {
        File file = File.createTempFile(fileName, "tmp");
        file.deleteOnExit();
        ThreeAddressGenerator threeAddressGenerator = new ThreeAddressGenerator();
        ThreeAddressProgram threeAddressProgram = program.accept(threeAddressGenerator);
        Codice generatedCode = new Codice(file.getPath());
        TACVariableAllocator tacVariableAllocator = new TACVariableAllocator(generatedCode);
        TACToMachineCompiler compiler = new TACToMachineCompiler(generatedCode, tacVariableAllocator);
        threeAddressProgram.accept(compiler);
        generatedCode.fineCodice();
        return file;
    }

    private Optional<Program> parseFromFile(String fileName) throws Exception {
        InputStream stream = Thread.currentThread().getContextClassLoader()
                .getResourceAsStream(fileName);
        InputStreamReader reader = new InputStreamReader(stream);
        ComplexSymbolFactory factory = new ComplexSymbolFactory();
        LanguageLexer lexer = new LanguageLexer(reader, factory, fileName);
        LanguageParser parser = new LanguageParser(lexer, factory, new ErrorReporter());

        Program result = (Program) parser.parse().value;
        UninitializedVariableDetector uninitializedVariableDetector = new UninitializedVariableDetector(new ErrorReporter());
        if (parser.errorCount() == 0)
            if (result.accept(uninitializedVariableDetector))
                return Optional.of(result);
        return Optional.empty();
    }
}
