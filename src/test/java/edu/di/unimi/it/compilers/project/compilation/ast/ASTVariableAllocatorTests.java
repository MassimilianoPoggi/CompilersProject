package edu.di.unimi.it.compilers.project.compilation.ast;

import edu.di.unimi.it.compilers.project.lexing.LanguageLexer;
import edu.di.unimi.it.compilers.project.parsing.LanguageParser;
import edu.di.unimi.it.compilers.project.parsing.ast.instructions.Program;
import java_cup.runtime.ComplexSymbolFactory;
import lt.macchina.Codice;
import lt.macchina.Macchina;
import org.junit.Test;

import java.io.StringReader;
import java.util.Optional;

import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.*;

public class ASTVariableAllocatorTests {
    @Test
    public void testAllocatesOnAssignmentInstruction() throws Exception {
        Program program = parseFromText("a = 5\nb=10\n").get();
        Codice code = mock(Codice.class);
        ASTVariableAllocator astVariableAllocator = new ASTVariableAllocator(code);
        program.accept(astVariableAllocator);

        assertTrue(astVariableAllocator.contains("a"));
        assertTrue(astVariableAllocator.contains("b"));
        assertNotEquals(astVariableAllocator.getLocation("a"), astVariableAllocator.getLocation("b"));
        verify(code, times(2)).genera(Macchina.PUSHIMM, 0);
    }

    @Test
    public void testAllocatesOnAssignmentExpression() throws Exception {
        Program program = parseFromText("output (a = 5) + (b = 10)\n").get();
        Codice code = mock(Codice.class);
        ASTVariableAllocator astVariableAllocator = new ASTVariableAllocator(code);
        program.accept(astVariableAllocator);

        assertTrue(astVariableAllocator.contains("a"));
        assertTrue(astVariableAllocator.contains("b"));
        assertNotEquals(astVariableAllocator.getLocation("a"), astVariableAllocator.getLocation("b"));
        verify(code, times(2)).genera(Macchina.PUSHIMM, 0);
    }

    @Test
    public void testAllocatesOnUsage() throws Exception {
        Program program = parseFromText("output a + b\n").get();
        Codice code = mock(Codice.class);
        ASTVariableAllocator astVariableAllocator = new ASTVariableAllocator(code);
        program.accept(astVariableAllocator);

        assertTrue(astVariableAllocator.contains("a"));
        assertTrue(astVariableAllocator.contains("b"));
        assertNotEquals(astVariableAllocator.getLocation("a"), astVariableAllocator.getLocation("b"));
        verify(code, times(2)).genera(Macchina.PUSHIMM, 0);
    }

    @Test
    public void testAllocatesOnDemand() {
        Codice code = mock(Codice.class);
        ASTVariableAllocator astVariableAllocator = new ASTVariableAllocator(code);
        astVariableAllocator.allocateVariable("%eax", astVariableAllocator.VARIABLE_SIZE);
        astVariableAllocator.allocateVariable("%ebx", astVariableAllocator.VARIABLE_SIZE);

        assertTrue(astVariableAllocator.contains("%eax"));
        assertTrue(astVariableAllocator.contains("%ebx"));
        assertNotEquals(astVariableAllocator.getLocation("%eax"), astVariableAllocator.getLocation("%ebx"));
        verify(code, times(2)).genera(Macchina.PUSHIMM, 0);
    }

    private Optional<Program> parseFromText(String text) throws Exception {
        StringReader reader = new StringReader(text);
        ComplexSymbolFactory factory = new ComplexSymbolFactory();
        LanguageLexer lexer = new LanguageLexer(reader, factory, "TestInput");
        LanguageParser parser = new LanguageParser(lexer, factory);

        Program result = (Program) parser.parse().value;
        return parser.errorCount() > 0 ? Optional.empty() : Optional.of(result);
    }
}
