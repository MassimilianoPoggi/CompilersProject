package edu.di.unimi.it.compilers.project.lexing;

import edu.di.unimi.it.compilers.project.parsing.LanguageParserSym;
import java_cup.runtime.ComplexSymbolFactory;
import java_cup.runtime.Symbol;
import org.junit.Test;

import java.io.StringReader;

import static org.junit.Assert.assertEquals;

public class LanguageLexerTests {
    @Test
    public void testWhitespace() throws Exception {
        LanguageLexer lexer = getLexer(" \t\f");
        assertEquals(LanguageParserSym.EOF, lexer.next_token().sym);
    }

    @Test
    public void testKeywords() throws Exception {
        LanguageLexer lexer = getLexer("output newLine loop endLoop input");
        assertEquals(LanguageParserSym.OUTPUT, lexer.next_token().sym);
        assertEquals(LanguageParserSym.NEWLINE, lexer.next_token().sym);
        assertEquals(LanguageParserSym.BEGIN_LOOP, lexer.next_token().sym);
        assertEquals(LanguageParserSym.END_LOOP, lexer.next_token().sym);
        assertEquals(LanguageParserSym.INPUT, lexer.next_token().sym);
        assertEquals(LanguageParserSym.EOF, lexer.next_token().sym);
    }

    @Test
    public void testSymbols() throws Exception {
        LanguageLexer lexer = getLexer("= + - * / % ( ) ? : \n");
        assertEquals(LanguageParserSym.EQUALS, lexer.next_token().sym);
        assertEquals(LanguageParserSym.PLUS, lexer.next_token().sym);
        assertEquals(LanguageParserSym.MINUS, lexer.next_token().sym);
        assertEquals(LanguageParserSym.TIMES, lexer.next_token().sym);
        assertEquals(LanguageParserSym.DIVIDE, lexer.next_token().sym);
        assertEquals(LanguageParserSym.MOD, lexer.next_token().sym);
        assertEquals(LanguageParserSym.LPAREN, lexer.next_token().sym);
        assertEquals(LanguageParserSym.RPAREN, lexer.next_token().sym);
        assertEquals(LanguageParserSym.QUESTION, lexer.next_token().sym);
        assertEquals(LanguageParserSym.COLON, lexer.next_token().sym);
        assertEquals(LanguageParserSym.CR, lexer.next_token().sym);
        assertEquals(LanguageParserSym.EOF, lexer.next_token().sym);
    }

    @Test
    public void testIdentifier() throws Exception {
        LanguageLexer lexer = getLexer("id123mixed456");
        Symbol id = lexer.next_token();

        assertEquals(LanguageParserSym.ID, id.sym);
        assertEquals("id123mixed456", id.value);
        assertEquals(LanguageParserSym.EOF, lexer.next_token().sym);
    }

    @Test
    public void testDecimalInteger() throws Exception {
        LanguageLexer lexer = getLexer("100");
        Symbol decimalNumber = lexer.next_token();

        assertEquals(LanguageParserSym.NUMBER, decimalNumber.sym);
        assertEquals(100, decimalNumber.value);
        assertEquals(LanguageParserSym.EOF, lexer.next_token().sym);
    }

    @Test
    public void testHexInteger() throws Exception {
        LanguageLexer lexer = getLexer("0x100");
        Symbol hexNumber = lexer.next_token();

        assertEquals(LanguageParserSym.NUMBER, hexNumber.sym);
        assertEquals(0x100, hexNumber.value);
        assertEquals(LanguageParserSym.EOF, lexer.next_token().sym);
    }

    @Test
    public void testString() throws Exception {
        LanguageLexer lexer = getLexer("\"prova 123\"");
        Symbol string = lexer.next_token();

        assertEquals(LanguageParserSym.STRING, string.sym);
        assertEquals("prova 123", string.value);
        assertEquals(LanguageParserSym.EOF, lexer.next_token().sym);
    }

    @Test
    public void testEscapeCharacters() throws Exception {
        LanguageLexer lexer = getLexer("\"\\t \\b \\n \\r \\f \\' \\\" \\\\\"");
        Symbol string = lexer.next_token();

        assertEquals(LanguageParserSym.STRING, string.sym);
        assertEquals("\t \b \n \r \f \' \" \\", string.value);
        assertEquals(LanguageParserSym.EOF, lexer.next_token().sym);
    }

    @Test
    public void testInvalidEscape() throws Exception {
        LanguageLexer lexer = getLexer("\"This\\s is going to get lexed\"");
        Symbol error = lexer.next_token();

        assertEquals(LanguageParserSym.error, error.sym);
        assertEquals("Invalid escape sequence: \\s", error.value);

        Symbol string = lexer.next_token();
        assertEquals(LanguageParserSym.STRING, string.sym);
        assertEquals("This is going to get lexed", string.value);

        assertEquals(LanguageParserSym.EOF, lexer.next_token().sym);
    }

    @Test
    public void testComment() throws Exception {
        LanguageLexer lexer = getLexer("// this is a comment");
        assertEquals(LanguageParserSym.EOF, lexer.next_token().sym);
    }

    @Test
    public void testLineContinuation() throws Exception {
        LanguageLexer lexer = getLexer("& \n");
        assertEquals(LanguageParserSym.EOF, lexer.next_token().sym);
    }

    @Test
    public void testLineContinuationWithComment() throws Exception {
        LanguageLexer lexer = getLexer("& //this is a comment \n");
        assertEquals(LanguageParserSym.EOF, lexer.next_token().sym);
    }

    @Test
    public void testUnrecognizedToken() throws Exception {
        LanguageLexer lexer = getLexer("!");
        Symbol error = lexer.next_token();

        assertEquals(LanguageParserSym.error, error.sym);
        assertEquals("unrecognized token !", error.value);
        assertEquals(LanguageParserSym.EOF, lexer.next_token().sym);
    }

    @Test
    public void testEmptyString() throws Exception {
        LanguageLexer lexer = getLexer("\"\"");
        Symbol error = lexer.next_token();

        assertEquals(LanguageParserSym.error, error.sym);
        assertEquals("empty string", error.value);
        assertEquals(LanguageParserSym.EOF, lexer.next_token().sym);
    }

    @Test
    public void testEOFDuringString() throws Exception {
        LanguageLexer lexer = getLexer("\"abc 123");
        Symbol error = lexer.next_token();

        assertEquals(LanguageParserSym.error, error.sym);
        assertEquals("EOF while parsing string", error.value);
        assertEquals(LanguageParserSym.EOF, lexer.next_token().sym);
    }

    @Test
    public void testNewlineDuringString() throws Exception {
        LanguageLexer lexer = getLexer("\"string with newline\n");
        Symbol error = lexer.next_token();

        assertEquals(LanguageParserSym.error, error.sym);
        assertEquals("newline while parsing string", error.value);
        assertEquals(LanguageParserSym.EOF, lexer.next_token().sym);
    }

    private LanguageLexer getLexer(String input) {
        StringReader reader = new StringReader(input);
        ComplexSymbolFactory factory = new ComplexSymbolFactory();
        return new LanguageLexer(reader, factory, "TestInput");
    }
}
