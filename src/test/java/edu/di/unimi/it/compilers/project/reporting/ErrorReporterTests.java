package edu.di.unimi.it.compilers.project.reporting;

import edu.di.unimi.it.compilers.project.lexing.LanguageLexer;
import edu.di.unimi.it.compilers.project.parsing.LanguageParser;
import edu.di.unimi.it.compilers.project.parsing.LanguageParserSym;
import java_cup.runtime.ComplexSymbolFactory;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.FileReader;
import java.io.PrintStream;
import java.net.URL;
import java.util.Arrays;
import java.util.Collections;

import static org.junit.Assert.assertEquals;

public class ErrorReporterTests {

    @Test
    public void tesLexerError() {
        ComplexSymbolFactory.ComplexSymbol symbol =
                getComplexSymbol("name", LanguageParserSym.error, "file", 10, 15, "error");

        ErrorReporter errorReporter = new ErrorReporter();

        assertEquals("file:10:15: error: error\n\tcannot read input file to show error",
                errorReporter.getSyntaxErrorMessage(symbol, Collections.emptyList()));
    }

    @Test
    public void testParserErrorValuelessToken() {
        ComplexSymbolFactory.ComplexSymbol symbol =
                getComplexSymbol("(", LanguageParserSym.LPAREN, "file", 10, 15, null);

        ErrorReporter errorReporter = new ErrorReporter();

        assertEquals("file:10:15: error: unexpected token '('\n" +
                        "\tcannot read input file to show error",
                errorReporter.getSyntaxErrorMessage(symbol, Collections.emptyList()));
    }

    @Test
    public void testParserErrorValueToken() {
        ComplexSymbolFactory.ComplexSymbol symbol =
                getComplexSymbol("identifier", LanguageParserSym.LPAREN, "file", 10, 15, "myid");

        ErrorReporter errorReporter = new ErrorReporter();

        assertEquals("file:10:15: error: unexpected token identifier with value 'myid'\n" +
                        "\tcannot read input file to show error",
                errorReporter.getSyntaxErrorMessage(symbol, Collections.emptyList()));
    }

    @Test
    public void testSyntaxErrorExpectedTokens() {
        ComplexSymbolFactory.ComplexSymbol symbol =
                getComplexSymbol("(", LanguageParserSym.LPAREN, "file", 10, 15, null);

        ErrorReporter errorReporter = new ErrorReporter();

        assertEquals("file:10:15: error: unexpected token '('\n" +
                        "\texpected tokens: ')', identifier\n" +
                        "\tcannot read input file to show error",
                errorReporter.getSyntaxErrorMessage(symbol, Arrays.asList(LanguageParserSym.RPAREN, LanguageParserSym.ID)));
    }

    @Test
    public void testSyntaxErrorWithRealFile() {
        URL fileLocation = Thread.currentThread().getContextClassLoader().getResource("bad.prog");
        ComplexSymbolFactory.ComplexSymbol symbol =
                getComplexSymbol("\\n", LanguageParserSym.CR, fileLocation.getFile(), 3, 34, null);

        ErrorReporter errorReporter = new ErrorReporter();

        assertEquals(fileLocation.getFile() + ":3:34: error: unexpected token '\\n'\n" +
                        "\t//segue resto di divisione intera\n" +
                        "\t                                 ^",
                errorReporter.getSyntaxErrorMessage(symbol, Collections.emptyList()));
    }

    @Test
    public void testParserIntegration() throws Exception {
        PrintStream stdErr = System.err;
        try {
            ByteArrayOutputStream myErr = new ByteArrayOutputStream();
            System.setErr(new PrintStream(myErr));

            URL fileLocation = Thread.currentThread().getContextClassLoader().getResource("bad.prog");
            ErrorReporter errorReporter = new ErrorReporter();
            ComplexSymbolFactory complexSymbolFactory = new ComplexSymbolFactory();
            LanguageLexer lexer = new LanguageLexer(new FileReader(fileLocation.getFile()),
                    complexSymbolFactory, fileLocation.getFile());
            LanguageParser parser = new LanguageParser(lexer, complexSymbolFactory, errorReporter);

            parser.parse();

            assertEquals(fileLocation.getFile() + ":3:34: error: unexpected token '\\n'\n" +
                            "\texpected tokens: '+', '-', '(', number, identifier\n" +
                            "\t//segue resto di divisione intera\n" +
                            "\t                                 ^",
                    myErr.toString().trim());
        } finally {
            System.setErr(stdErr);
        }
    }

    @Test
    public void testWarning() {
        ComplexSymbolFactory.Location warningLocation = new ComplexSymbolFactory.Location("TestUnit", 1, 8);

        ErrorReporter errorReporter = new ErrorReporter();
        assertEquals("TestUnit:1:8: warning: my warning message\n" +
                        "\tcannot read input file to show error",
                errorReporter.getWarningMessage("my warning message", warningLocation));
    }

    @Test
    public void testWarningWithRealFile() {
        URL fileLocation = Thread.currentThread().getContextClassLoader().getResource("bad.prog");
        ErrorReporter errorReporter = new ErrorReporter();

        ComplexSymbolFactory.Location warningLocation =
                new ComplexSymbolFactory.Location(fileLocation.getFile(), 4, 1);

        assertEquals(fileLocation.getFile() + ":4:1: warning: my warning message\n" +
                        "\tx % y\n" +
                        "\t^",
                errorReporter.getWarningMessage("my warning message", warningLocation));
    }

    private ComplexSymbolFactory.ComplexSymbol getComplexSymbol(String name, int id, String unit,
                                                                int line, int column, Object value) {
        ComplexSymbolFactory factory = new ComplexSymbolFactory();
        return (ComplexSymbolFactory.ComplexSymbol)
                factory.newSymbol(name, id,
                        new ComplexSymbolFactory.Location(unit, line, column),
                        new ComplexSymbolFactory.Location(unit, line, column + 10),
                        value);
    }
}
