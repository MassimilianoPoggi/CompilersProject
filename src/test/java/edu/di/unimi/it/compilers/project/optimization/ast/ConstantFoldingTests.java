package edu.di.unimi.it.compilers.project.optimization.ast;

import edu.di.unimi.it.compilers.project.lexing.LanguageLexer;
import edu.di.unimi.it.compilers.project.parsing.LanguageParser;
import edu.di.unimi.it.compilers.project.parsing.ast.instructions.Program;
import java_cup.runtime.ComplexSymbolFactory;
import org.junit.Test;

import java.io.StringReader;
import java.util.Optional;

import static org.junit.Assert.assertEquals;

public class ConstantFoldingTests {
    @Test
    public void eliminatesConstantExpression() throws Exception {
        Program redundant = parseFromText("output 5 + 5\n").get();
        Program simplified = parseFromText("output 10\n").get();

        ConstantFolding constantFolding = new ConstantFolding();

        assertEquals(simplified, constantFolding.visitProgram(redundant));
    }

    @Test
    public void doesNotEliminateUnknownExpression() throws Exception {
        Program notRedundant = parseFromText("output input + 5\n").get();

        ConstantFolding constantFolding = new ConstantFolding();

        assertEquals(notRedundant, constantFolding.visitProgram(notRedundant));
    }

    private Optional<Program> parseFromText(String text) throws Exception {
        StringReader reader = new StringReader(text);
        ComplexSymbolFactory factory = new ComplexSymbolFactory();
        LanguageLexer lexer = new LanguageLexer(reader, factory, "TestInput");
        LanguageParser parser = new LanguageParser(lexer, factory);

        Program result = (Program) parser.parse().value;
        return parser.errorCount() > 0 ? Optional.empty() : Optional.of(result);
    }
}
