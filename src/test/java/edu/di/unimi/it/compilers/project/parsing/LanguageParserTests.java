package edu.di.unimi.it.compilers.project.parsing;

import edu.di.unimi.it.compilers.project.lexing.LanguageLexer;
import edu.di.unimi.it.compilers.project.parsing.ast.instructions.Program;
import java_cup.runtime.ComplexSymbolFactory;
import org.junit.Test;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.util.Optional;

import static org.junit.Assert.*;

public class LanguageParserTests {
    @Test
    public void testUnaryPrecedence() throws Exception {
        Program ambiguous = parseFromText("id = - 1 * 2\n").get();
        Program correct = parseFromText("id = (- 1) * 2\n").get();
        Program incorrect = parseFromText("id = - (1 * 2)\n").get();

        assertEquals(correct, ambiguous);
        assertNotEquals(incorrect, ambiguous);
    }

    @Test
    public void testMultiplicationPrecedence() throws Exception {
        Program ambiguous = parseFromText("id = 1 + 2 * 3\n").get();
        Program correct = parseFromText("id = 1 + (2 * 3)\n").get();
        Program incorrect = parseFromText("id = (1 + 2) * 3\n").get();

        assertEquals(correct, ambiguous);
        assertNotEquals(incorrect, ambiguous);
    }

    @Test
    public void testAdditionPrecedence() throws Exception {
        Program ambiguous = parseFromText("id = 1 + 2 ? 3 : 4 + 5\n").get();
        Program correct = parseFromText("id = (1 + 2) ? 3 : (4 + 5)\n").get();
        Program incorrect1 = parseFromText("id = 1 + (2 ? 3 : 4) + 5\n").get();
        Program incorrect2 = parseFromText("id = ((1 + 2) ? 3 : 4) +5\n").get();
        Program incorrect3 = parseFromText("id = 1 + (2 ? 3 : (4 + 5))\n").get();

        assertEquals(correct, ambiguous);
        assertNotEquals(incorrect1, ambiguous);
        assertNotEquals(incorrect2, ambiguous);
        assertNotEquals(incorrect3, ambiguous);
    }

    @Test
    public void testTernaryPrecedence() throws Exception {
        Program ambiguous = parseFromText("id1 = id2 = 1 ? 2 : 3\n").get();
        Program correct = parseFromText("id1 = id2 = (1 ? 2 : 3)\n").get();
        Program incorrect = parseFromText("id1 = (id2 = 1) ? 2 : 3\n").get();

        assertEquals(correct, ambiguous);
        assertNotEquals(incorrect, ambiguous);
    }

    @Test
    public void testMultiplicationAssociativity() throws Exception {
        Program ambiguous = parseFromText("id = 1 * 2 * 3\n").get();
        Program correct = parseFromText("id = (1 * 2) * 3\n").get();
        Program incorrect = parseFromText("id = 1 * (2 * 3)\n").get();

        assertEquals(correct, ambiguous);
        assertNotEquals(incorrect, ambiguous);
    }

    @Test
    public void testAdditionAssociativity() throws Exception {
        Program ambiguous = parseFromText("id = 1 + 2 + 3\n").get();
        Program correct = parseFromText("id = (1 + 2) + 3\n").get();
        Program incorrect = parseFromText("id = 1 + (2 + 3)\n").get();

        assertEquals(correct, ambiguous);
        assertNotEquals(incorrect, ambiguous);
    }

    @Test
    public void testTernaryAssociativity() throws Exception {
        Program ambiguous = parseFromText("id = 1 ? 2 : 3 ? 4 : 5\n").get();
        Program correct = parseFromText("id = 1 ? 2 : (3 ? 4 : 5)\n").get();
        Program incorrect = parseFromText("id = (1 ? 2 : 3) ? 4 : 5\n").get();

        assertEquals(correct, ambiguous);
        assertNotEquals(incorrect, ambiguous);
    }

    @Test
    public void testMCDProgram() throws Exception {
        assertTrue(parseFromFile("MCD.prog").isPresent());
    }

    @Test
    public void testMultProgram() throws Exception {
        assertTrue(parseFromFile("mult.prog").isPresent());
    }

    @Test
    public void testEvenProgram() throws Exception {
        assertTrue(parseFromFile("even.prog").isPresent());
    }

    @Test
    public void testMinProgram() throws Exception {
        assertTrue(parseFromFile("min.prog").isPresent());
    }

    @Test
    public void testIncorrectProgram() throws Exception {
        assertFalse(parseFromFile("bad.prog").isPresent());
    }

    private Optional<Program> parseFromText(String text) throws Exception {
        StringReader reader = new StringReader(text);
        ComplexSymbolFactory factory = new ComplexSymbolFactory();
        LanguageLexer lexer = new LanguageLexer(reader, factory, "TestInput");
        LanguageParser parser = new LanguageParser(lexer, factory);

        Program result = (Program) parser.parse().value;
        return parser.errorCount() > 0 ? Optional.empty() : Optional.of(result);
    }

    private Optional<Program> parseFromFile(String fileName) throws Exception {
        InputStream stream = Thread.currentThread().getContextClassLoader()
                .getResourceAsStream(fileName);
        InputStreamReader reader = new InputStreamReader(stream);
        ComplexSymbolFactory factory = new ComplexSymbolFactory();
        LanguageLexer lexer = new LanguageLexer(reader, factory, fileName);
        LanguageParser parser = new LanguageParser(lexer, factory);

        Program result = (Program) parser.parse().value;
        return parser.errorCount() > 0 ? Optional.empty() : Optional.of(result);
    }
}