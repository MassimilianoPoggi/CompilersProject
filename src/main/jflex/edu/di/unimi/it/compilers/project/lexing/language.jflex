package edu.di.unimi.it.compilers.project.lexing;

import edu.di.unimi.it.compilers.project.parsing.LanguageParserSym;

import java_cup.runtime.ComplexSymbolFactory;
import java_cup.runtime.Symbol;

%%

%unicode
%public
%class LanguageLexer
%cup
%line
%column

%{
    private ComplexSymbolFactory symbolFactory;
    private StringBuilder stringBuilder;
    private String sourceFile;

    public LanguageLexer(java.io.Reader in, ComplexSymbolFactory symbolFactory, String sourceFile) {
        this(in);
        this.stringBuilder = new StringBuilder();
        this.symbolFactory = symbolFactory;
        this.sourceFile = sourceFile;
    }

    private ComplexSymbolFactory.Location startLocation() {
        return new ComplexSymbolFactory.Location(sourceFile, yyline+1, yycolumn+1);
    }

    private ComplexSymbolFactory.Location endLocation() {
        return new ComplexSymbolFactory.Location(sourceFile, yyline+1, yycolumn+yylength());
    }

    private Symbol token(String name, int tokenType) {
        return symbolFactory.newSymbol(name, tokenType, startLocation(), endLocation());
    }

    private Symbol token(String name, int tokenType, Object value) {
        return symbolFactory.newSymbol(name, tokenType, startLocation(), endLocation(), value);
    }
%}

%state STRING

NEWLINE = "\r" | "\n" | "\r\n"
SPACE = [ \t\f]
COMMENT = "//" [^\r\n]*
ID = [:jletter:][:jletterdigit:]*
DECIMAL_INTEGER = "0" | [1-9][0-9]*
HEX_INTEGER = "0x" ( "0" | [1-9a-fA-F][0-9a-fA-F]* )

%%

<YYINITIAL> {
    "output"                              { return token(yytext(), LanguageParserSym.OUTPUT); }
    "newLine"                             { return token(yytext(), LanguageParserSym.NEWLINE); }
    "loop"                                { return token(yytext(), LanguageParserSym.BEGIN_LOOP); }
    "endLoop"                             { return token(yytext(), LanguageParserSym.END_LOOP); }
    "input"                               { return token(yytext(), LanguageParserSym.INPUT); }
    "="                                   { return token(yytext(), LanguageParserSym.EQUALS); }
    "+"                                   { return token(yytext(), LanguageParserSym.PLUS); }
    "-"                                   { return token(yytext(), LanguageParserSym.MINUS); }
    "*"                                   { return token(yytext(), LanguageParserSym.TIMES); }
    "/"                                   { return token(yytext(), LanguageParserSym.DIVIDE); }
    "%"                                   { return token(yytext(), LanguageParserSym.MOD); }
    "("                                   { return token(yytext(), LanguageParserSym.LPAREN); }
    ")"                                   { return token(yytext(), LanguageParserSym.RPAREN); }
    "?"                                   { return token(yytext(), LanguageParserSym.QUESTION); }
    ":"                                   { return token(yytext(), LanguageParserSym.COLON); }
    {NEWLINE}                             { return token("\\n", LanguageParserSym.CR); }
    {ID}                                  { return token("identifier", LanguageParserSym.ID, yytext()); }
    {DECIMAL_INTEGER}                     { return token("number", LanguageParserSym.NUMBER,
                                                Integer.valueOf(yytext(), 10)); }
    {HEX_INTEGER}                         { return token("number", LanguageParserSym.NUMBER,
                                                Integer.valueOf(yytext().substring(2), 16)); }
    "&" {SPACE}* {COMMENT}? {NEWLINE}     {}
    {COMMENT}                             {}
    {SPACE}                               {}
    "\""                                  { stringBuilder.setLength(0); yybegin(STRING); }
}

<STRING> {
    "\""                                  {
                                            yybegin(YYINITIAL);
                                            if (stringBuilder.length() == 0)
                                                return token("ERROR", LanguageParserSym.error, "empty string");

                                            String string = stringBuilder.toString();
                                            return token("string", LanguageParserSym.STRING, string);
                                          }

    [^\n\r\"\\]+                          { stringBuilder.append( yytext() ); }

    "\\t"                                 { stringBuilder.append("\t"); }
    "\\b"                                 { stringBuilder.append("\b"); }
    "\\n"                                 { stringBuilder.append("\n"); }
    "\\r"                                 { stringBuilder.append("\r"); }
    "\\f"                                 { stringBuilder.append("\f"); }
    "\\'"                                 { stringBuilder.append("\'"); }
    "\\\""                                { stringBuilder.append("\""); }
    "\\\\"                                { stringBuilder.append("\\"); }

    "\\".                                 { return token("ERROR", LanguageParserSym.error,
                                                "Invalid escape sequence: " + yytext()); }

    {NEWLINE}                             {
                                            yybegin(YYINITIAL);
                                            return token("ERROR", LanguageParserSym.error, "newline while parsing string");
                                          }

    <<EOF>>                               {
                                            yybegin(YYINITIAL);
                                            return token("ERROR", LanguageParserSym.error, "EOF while parsing string");
                                          }
}

.                                         { return token("ERROR", LanguageParserSym.error, "unrecognized token " + yytext()); }
<<EOF>>                                   { return token("EOF", LanguageParserSym.EOF); }