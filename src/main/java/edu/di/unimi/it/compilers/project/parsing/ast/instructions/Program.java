package edu.di.unimi.it.compilers.project.parsing.ast.instructions;

import edu.di.unimi.it.compilers.project.parsing.ast.InstructionVisitor;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

public class Program implements Instruction {
    private final List<Instruction> instructionList;

    public Program(List<Instruction> instructionList) {
        this.instructionList = instructionList;
    }

    public List<Instruction> getInstructionList() {
        return Collections.unmodifiableList(instructionList);
    }

    @Override
    public <T> T accept(InstructionVisitor<T> instructionVisitor) {
        return instructionVisitor.visitProgram(this);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Program program = (Program) o;
        return Objects.equals(instructionList, program.instructionList);
    }

    @Override
    public int hashCode() {
        return Objects.hash(instructionList);
    }
}
