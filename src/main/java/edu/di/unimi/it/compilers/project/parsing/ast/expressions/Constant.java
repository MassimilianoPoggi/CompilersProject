package edu.di.unimi.it.compilers.project.parsing.ast.expressions;

import edu.di.unimi.it.compilers.project.parsing.ast.ExpressionVisitor;

import java.util.Objects;

public class Constant implements Expression {
    private final Integer value;

    public Constant(Integer value) {
        this.value = value;
    }

    public Integer getValue() {
        return value;
    }

    @Override
    public <T> T accept(ExpressionVisitor<T> expressionVisitor) {
        return expressionVisitor.visitConstant(this);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Constant constant = (Constant) o;
        return Objects.equals(value, constant.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(value);
    }
}
