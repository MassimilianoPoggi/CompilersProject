package edu.di.unimi.it.compilers.project.parsing.ast.expressions;

import edu.di.unimi.it.compilers.project.parsing.ast.ExpressionVisitor;

import java.util.Objects;

public class DivExpression implements Expression {
    private final Expression leftExpression;
    private final Expression rightExpression;

    public DivExpression(Expression leftExpression, Expression rightExpression) {
        this.leftExpression = leftExpression;
        this.rightExpression = rightExpression;
    }

    public Expression getLeftExpression() {
        return leftExpression;
    }

    public Expression getRightExpression() {
        return rightExpression;
    }

    @Override
    public <T> T accept(ExpressionVisitor<T> expressionVisitor) {
        return expressionVisitor.visitDiv(this);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DivExpression that = (DivExpression) o;
        return Objects.equals(leftExpression, that.leftExpression) &&
                Objects.equals(rightExpression, that.rightExpression);
    }

    @Override
    public int hashCode() {
        return Objects.hash(leftExpression, rightExpression);
    }
}
