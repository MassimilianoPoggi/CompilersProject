package edu.di.unimi.it.compilers.project.representations.tac.instructions;

import java.util.Objects;

public class ThreeAddressConstant implements ThreeAddressOperand {
    private final int value;

    public ThreeAddressConstant(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    @Override
    public String toString() {
        return String.valueOf(value);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ThreeAddressConstant that = (ThreeAddressConstant) o;
        return value == that.value;
    }

    @Override
    public int hashCode() {
        return Objects.hash(value);
    }
}
