package edu.di.unimi.it.compilers.project.compilation.ast;

import edu.di.unimi.it.compilers.project.parsing.ast.ASTVisitor;
import edu.di.unimi.it.compilers.project.parsing.ast.expressions.*;
import edu.di.unimi.it.compilers.project.parsing.ast.instructions.*;
import lt.macchina.Codice;
import lt.macchina.Macchina;

import java.util.Optional;

public class ASTToMachineCompiler implements ASTVisitor<Codice, Codice> {
    private final Codice generatedCode;
    private final ASTVariableAllocator astVariableAllocator;

    public ASTToMachineCompiler(Codice generatedCode, ASTVariableAllocator astVariableAllocator) {
        this.generatedCode = generatedCode;
        this.astVariableAllocator = astVariableAllocator;
    }

    @Override
    public Codice visitAdd(AddExpression addExpression) {
        addExpression.getLeftExpression().accept(this);
        addExpression.getRightExpression().accept(this);
        generatedCode.genera(Macchina.ADD);
        return generatedCode;
    }

    @Override
    public Codice visitAssignment(AssignmentExpression assignmentExpression) {
        assignmentExpression.getExpression().accept(this);
        int variableLocation = astVariableAllocator.getLocation(assignmentExpression.getId());
        generatedCode.genera(Macchina.POP, variableLocation);
        generatedCode.genera(Macchina.PUSH, variableLocation);
        return generatedCode;
    }

    @Override
    public Codice visitConstant(Constant constant) {
        generatedCode.genera(Macchina.PUSHIMM, constant.getValue());
        return generatedCode;
    }

    @Override
    public Codice visitDiv(DivExpression divExpression) {
        divExpression.getLeftExpression().accept(this);
        divExpression.getRightExpression().accept(this);
        generatedCode.genera(Macchina.DIV);
        return generatedCode;
    }

    @Override
    public Codice visitInput(InputExpression inputExpression) {
        Optional<String> message = inputExpression.getString();
        printString(message);
        generatedCode.genera(Macchina.INPUT);
        return generatedCode;
    }

    @Override
    public Codice visitMod(ModExpression modExpression) {
        modExpression.getLeftExpression().accept(this);
        generatedCode.genera(Macchina.PUSHIMM, 0);
        pushSPRelativeAddress(-2);
        generatedCode.genera(Macchina.PUSHIND);
        pushSPRelativeAddress(-2);
        modExpression.getRightExpression().accept(this);
        generatedCode.genera(Macchina.POPIND);
        pushSPRelativeAddress(-2);
        generatedCode.genera(Macchina.PUSHIND);
        generatedCode.genera(Macchina.DIV);
        generatedCode.genera(Macchina.MUL);
        generatedCode.genera(Macchina.SUB);
        return generatedCode;
    }

    private void pushSPRelativeAddress(int offset) {
        generatedCode.genera(Macchina.PUSHSP);
        generatedCode.genera(Macchina.PUSHIMM, offset);
        generatedCode.genera(Macchina.ADD);
    }

    @Override
    public Codice visitMult(MultExpression multExpression) {
        multExpression.getLeftExpression().accept(this);
        multExpression.getRightExpression().accept(this);
        generatedCode.genera(Macchina.MUL);
        return generatedCode;
    }

    @Override
    public Codice visitNegative(NegativeExpression negativeExpression) {
        negativeExpression.getExpression().accept(this);
        generatedCode.genera(Macchina.PUSHIMM, -1);
        generatedCode.genera(Macchina.MUL);
        return generatedCode;
    }

    @Override
    public Codice visitSub(SubExpression subExpression) {
        subExpression.getLeftExpression().accept(this);
        subExpression.getRightExpression().accept(this);
        generatedCode.genera(Macchina.SUB);
        return generatedCode;
    }

    @Override
    public Codice visitTernary(TernaryExpression ternaryExpression) {
        ternaryExpression.getCondition().accept(this);
        int ternaryJump = generatedCode.generaParziale(Macchina.JZERO);
        ternaryExpression.getTrueExpression().accept(this);
        int expressionJump = generatedCode.generaParziale(Macchina.JUMP);
        generatedCode.completaIstruzione(ternaryJump, generatedCode.indirizzoProssimaIstruzione());
        ternaryExpression.getFalseExpression().accept(this);
        generatedCode.completaIstruzione(expressionJump, generatedCode.indirizzoProssimaIstruzione());
        return generatedCode;
    }

    @Override
    public Codice visitVariable(Variable variable) {
        generatedCode.genera(Macchina.PUSH, astVariableAllocator.getLocation(variable.getId()));
        return generatedCode;
    }

    @Override
    public Codice visitAssignment(AssignmentInstruction assignmentInstruction) {
        assignmentInstruction.getExpression().accept(this);
        generatedCode.genera(Macchina.POP, astVariableAllocator.getLocation(assignmentInstruction.getId()));
        return generatedCode;
    }

    @Override
    public Codice visitEmpty(EmptyInstruction emptyInstruction) {
        return generatedCode;
    }

    @Override
    public Codice visitLoop(LoopInstruction loopInstruction) {
        int loopStart = generatedCode.indirizzoProssimaIstruzione();
        loopInstruction.getCondition().accept(this);
        int loopJump = generatedCode.generaParziale(Macchina.JZERO);
        for (Instruction instruction : loopInstruction.getInstructionList())
            instruction.accept(this);
        generatedCode.genera(Macchina.JUMP, loopStart);
        generatedCode.completaIstruzione(loopJump, generatedCode.indirizzoProssimaIstruzione());
        return generatedCode;
    }

    @Override
    public Codice visitOutput(OutputInstruction outputInstruction) {
        Optional<String> message = outputInstruction.getString();
        printString(message);

        Optional<Expression> expression = outputInstruction.getExpression();
        if (expression.isPresent()) {
            expression.get().accept(this);
            generatedCode.genera(Macchina.OUTPUT);
        }

        return generatedCode;
    }

    @Override
    public Codice visitProgram(Program program) {
        program.accept(astVariableAllocator);
        for (Instruction instruction : program.getInstructionList())
            instruction.accept(this);
        generatedCode.genera(Macchina.HALT);
        return generatedCode;
    }

    private void printString(Optional<String> message) {
        if (message.isPresent()) {
            for (char c : message.get().toCharArray()) {
                generatedCode.genera(Macchina.PUSHIMM, c);
                generatedCode.genera(Macchina.OUTPUTCH);
            }
        }
    }
}
