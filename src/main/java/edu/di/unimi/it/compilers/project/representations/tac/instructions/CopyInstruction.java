package edu.di.unimi.it.compilers.project.representations.tac.instructions;

import edu.di.unimi.it.compilers.project.representations.tac.ThreeAddressCodeVisitor;

import java.util.Objects;

public class CopyInstruction implements ThreeAddressInstruction {
    private final ThreeAddressVariable result;
    private final ThreeAddressOperand expression;

    public CopyInstruction(ThreeAddressVariable result, ThreeAddressOperand expression) {
        this.result = result;
        this.expression = expression;
    }

    public ThreeAddressVariable getResult() {
        return result;
    }

    public ThreeAddressOperand getExpression() {
        return expression;
    }

    @Override
    public <T> T accept(ThreeAddressCodeVisitor<T> visitor) {
        return visitor.visitCopy(this);
    }

    @Override
    public String toString() {
        return result + " = " + expression;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CopyInstruction that = (CopyInstruction) o;
        return Objects.equals(result, that.result) &&
                Objects.equals(expression, that.expression);
    }

    @Override
    public int hashCode() {
        return Objects.hash(result, expression);
    }
}
