package edu.di.unimi.it.compilers.project.parsing.ast.instructions;

import edu.di.unimi.it.compilers.project.parsing.ast.InstructionVisitor;
import edu.di.unimi.it.compilers.project.parsing.ast.expressions.Expression;

import java.util.Objects;
import java.util.Optional;

public class OutputInstruction implements Instruction {
    private final Optional<String> string;
    private final Optional<Expression> expression;

    public OutputInstruction(Optional<String> string, Optional<Expression> expression) {
        this.string = string;
        this.expression = expression;
    }

    public Optional<String> getString() {
        return string;
    }

    public Optional<Expression> getExpression() {
        return expression;
    }

    @Override
    public <T> T accept(InstructionVisitor<T> instructionVisitor) {
        return instructionVisitor.visitOutput(this);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        OutputInstruction that = (OutputInstruction) o;
        return Objects.equals(string, that.string) &&
                Objects.equals(expression, that.expression);
    }

    @Override
    public int hashCode() {
        return Objects.hash(string, expression);
    }
}
