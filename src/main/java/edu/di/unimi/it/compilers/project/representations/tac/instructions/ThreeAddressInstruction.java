package edu.di.unimi.it.compilers.project.representations.tac.instructions;

import edu.di.unimi.it.compilers.project.representations.ssa.instructions.SSAInstruction;
import edu.di.unimi.it.compilers.project.representations.tac.ThreeAddressCodeVisitor;

public interface ThreeAddressInstruction extends SSAInstruction {
    <T> T accept(ThreeAddressCodeVisitor<T> visitor);
}
