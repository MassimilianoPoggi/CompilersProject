package edu.di.unimi.it.compilers.project.representations.tac;

import edu.di.unimi.it.compilers.project.parsing.ast.ASTVisitor;
import edu.di.unimi.it.compilers.project.parsing.ast.expressions.*;
import edu.di.unimi.it.compilers.project.parsing.ast.instructions.*;
import edu.di.unimi.it.compilers.project.parsing.ast.instructions.OutputInstruction;
import edu.di.unimi.it.compilers.project.representations.tac.instructions.*;

import java.util.Optional;

public class ThreeAddressGenerator implements ASTVisitor<ThreeAddressOperand, ThreeAddressProgram> {
    private final ThreeAddressProgram generatedProgram;
    private int varCount;

    public ThreeAddressGenerator() {
        generatedProgram = new ThreeAddressProgram();
        varCount = 0;
    }

    private ThreeAddressVariable generateNewVariable() {
        return new ThreeAddressVariable("%var" + varCount++);
    }

    @Override
    public ThreeAddressVariable visitAdd(AddExpression addExpression) {
        ThreeAddressOperand left = addExpression.getLeftExpression().accept(this);
        ThreeAddressOperand right = addExpression.getRightExpression().accept(this);
        ThreeAddressVariable result = generateNewVariable();
        generatedProgram.addInstruction(new AddInstruction(result, left, right));
        return result;
    }

    @Override
    public ThreeAddressVariable visitAssignment(AssignmentExpression assignmentExpression) {
        ThreeAddressOperand expression = assignmentExpression.getExpression().accept(this);
        ThreeAddressVariable result = new ThreeAddressVariable(assignmentExpression.getId());
        generatedProgram.addInstruction(new CopyInstruction(result, expression));
        return result;
    }

    @Override
    public ThreeAddressConstant visitConstant(Constant constant) {
        return new ThreeAddressConstant(constant.getValue());
    }

    @Override
    public ThreeAddressVariable visitDiv(DivExpression divExpression) {
        ThreeAddressOperand left = divExpression.getLeftExpression().accept(this);
        ThreeAddressOperand right = divExpression.getRightExpression().accept(this);
        ThreeAddressVariable result = generateNewVariable();
        generatedProgram.addInstruction(new DivInstruction(result, left, right));
        return result;
    }

    @Override
    public ThreeAddressVariable visitInput(InputExpression inputExpression) {
        ThreeAddressVariable result = generateNewVariable();
        generatedProgram.addInstruction(new InputInstruction(result, inputExpression.getString()));
        return result;
    }

    @Override
    public ThreeAddressVariable visitMod(ModExpression modExpression) {
        ThreeAddressOperand left = modExpression.getLeftExpression().accept(this);
        ThreeAddressOperand right = modExpression.getRightExpression().accept(this);
        ThreeAddressVariable divResult = generateNewVariable();
        generatedProgram.addInstruction(new DivInstruction(divResult, left, right));
        ThreeAddressVariable mulResult = generateNewVariable();
        generatedProgram.addInstruction(new MultInstruction(mulResult, divResult, right));
        ThreeAddressVariable modResult = generateNewVariable();
        generatedProgram.addInstruction(new SubInstruction(modResult, left, mulResult));
        return modResult;
    }

    @Override
    public ThreeAddressVariable visitMult(MultExpression multExpression) {
        ThreeAddressOperand left = multExpression.getLeftExpression().accept(this);
        ThreeAddressOperand right = multExpression.getRightExpression().accept(this);
        ThreeAddressVariable result = generateNewVariable();
        generatedProgram.addInstruction(new MultInstruction(result, left, right));
        return result;
    }

    @Override
    public ThreeAddressVariable visitNegative(NegativeExpression negativeExpression) {
        ThreeAddressOperand expression = negativeExpression.getExpression().accept(this);
        ThreeAddressVariable result = generateNewVariable();
        generatedProgram.addInstruction(new MultInstruction(result, new ThreeAddressConstant(-1), expression));
        return result;
    }

    @Override
    public ThreeAddressVariable visitSub(SubExpression subExpression) {
        ThreeAddressOperand left = subExpression.getLeftExpression().accept(this);
        ThreeAddressOperand right = subExpression.getRightExpression().accept(this);
        ThreeAddressVariable result = generateNewVariable();
        generatedProgram.addInstruction(new SubInstruction(result, left, right));
        return result;
    }

    @Override
    public ThreeAddressVariable visitTernary(TernaryExpression ternaryExpression) {
        ThreeAddressOperand condition = ternaryExpression.getCondition().accept(this);
        ThreeAddressVariable result = generateNewVariable();
        JumpZeroInstruction jumpToFalse = new JumpZeroInstruction(condition);
        generatedProgram.addInstruction(jumpToFalse);

        ThreeAddressOperand trueExpression = ternaryExpression.getTrueExpression().accept(this);
        generatedProgram.addInstruction(new CopyInstruction(result, trueExpression));
        JumpInstruction jumpToEnd = new JumpInstruction();
        generatedProgram.addInstruction(jumpToEnd);

        jumpToFalse.setJumpAddress(generatedProgram.getNextInstructionAddress());
        ThreeAddressOperand falseExpression = ternaryExpression.getFalseExpression().accept(this);
        generatedProgram.addInstruction(new CopyInstruction(result, falseExpression));

        jumpToEnd.setJumpAddress(generatedProgram.getNextInstructionAddress());
        return result;
    }

    @Override
    public ThreeAddressVariable visitVariable(Variable variable) {
        return new ThreeAddressVariable(variable.getId());
    }

    @Override
    public ThreeAddressProgram visitAssignment(AssignmentInstruction assignmentInstruction) {
        ThreeAddressOperand expression = assignmentInstruction.getExpression().accept(this);
        ThreeAddressVariable result = new ThreeAddressVariable(assignmentInstruction.getId());
        generatedProgram.addInstruction(new CopyInstruction(result, expression));
        return this.generatedProgram;
    }

    @Override
    public ThreeAddressProgram visitEmpty(EmptyInstruction emptyInstruction) {
        return this.generatedProgram;
    }

    @Override
    public ThreeAddressProgram visitLoop(LoopInstruction loopInstruction) {
        int conditionAddress = generatedProgram.getNextInstructionAddress();
        ThreeAddressOperand condition = loopInstruction.getCondition().accept(this);
        JumpZeroInstruction jumpToEnd = new JumpZeroInstruction(condition);
        generatedProgram.addInstruction(jumpToEnd);
        for (Instruction instruction : loopInstruction.getInstructionList())
            instruction.accept(this);
        generatedProgram.addInstruction(new JumpInstruction(conditionAddress));
        jumpToEnd.setJumpAddress(generatedProgram.getNextInstructionAddress());
        return this.generatedProgram;
    }

    @Override
    public ThreeAddressProgram visitOutput(OutputInstruction outputInstruction) {
        Optional<Expression> expression = outputInstruction.getExpression();
        if (expression.isPresent()) {
            ThreeAddressOperand expressionOperand = expression.get().accept(this);
            generatedProgram.addInstruction(
                    new edu.di.unimi.it.compilers.project.representations.tac.instructions.OutputInstruction
                            (outputInstruction.getString(), Optional.of(expressionOperand)));
        } else
            generatedProgram.addInstruction(
                    new edu.di.unimi.it.compilers.project.representations.tac.instructions.OutputInstruction
                            (outputInstruction.getString(), Optional.empty()));

        return this.generatedProgram;
    }

    @Override
    public ThreeAddressProgram visitProgram(Program program) {
        for (Instruction instruction : program.getInstructionList())
            instruction.accept(this);

        generatedProgram.addInstruction(new HaltInstruction());
        return this.generatedProgram;
    }
}
