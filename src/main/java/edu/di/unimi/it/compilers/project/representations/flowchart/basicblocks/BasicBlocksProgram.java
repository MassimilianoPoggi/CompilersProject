package edu.di.unimi.it.compilers.project.representations.flowchart.basicblocks;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class BasicBlocksProgram<T> {
    private final List<BasicBlock<T>> basicBlocks;

    public BasicBlocksProgram(List<BasicBlock<T>> basicBlocks) {
        this.basicBlocks = basicBlocks;
    }

    public List<BasicBlock<T>> getBasicBlocks() {
        return Collections.unmodifiableList(basicBlocks);
    }

    public BasicBlock<T> getBlockForInstruction(int index) {
        return basicBlocks
                .stream()
                .filter(b -> b.containsInstruction(index))
                .findFirst()
                .orElseThrow(() -> {
                    System.out.println(this.toString());
                    return new IllegalArgumentException("No block found for instruction " + index);
                });
    }

    @Override
    public String toString() {
        return basicBlocks
                .stream()
                .map(Object::toString)
                .collect(Collectors.joining("\n#######################################\n"));
    }
}
