package edu.di.unimi.it.compilers.project.representations.tac;

import edu.di.unimi.it.compilers.project.representations.tac.instructions.*;

public interface ThreeAddressCodeVisitor<T> {
    T visitAdd(AddInstruction addInstruction);

    T visitCopy(CopyInstruction copyInstruction);

    T visitDiv(DivInstruction divInstruction);

    T visitHalt(HaltInstruction haltInstruction);

    T visitInput(InputInstruction inputInstruction);

    T visitJump(JumpInstruction jumpInstruction);

    T visitJumpZero(JumpZeroInstruction jumpZeroInstruction);

    T visitMult(MultInstruction multInstruction);

    T visitOutput(OutputInstruction outputInstruction);

    T visitSub(SubInstruction subInstruction);

    T visitProgram(ThreeAddressProgram program);
}
