package edu.di.unimi.it.compilers.project.warnings;

import edu.di.unimi.it.compilers.project.parsing.ast.ASTVisitor;
import edu.di.unimi.it.compilers.project.parsing.ast.expressions.*;
import edu.di.unimi.it.compilers.project.parsing.ast.instructions.*;
import edu.di.unimi.it.compilers.project.reporting.ErrorReporter;
import edu.di.unimi.it.compilers.project.utils.SymbolTable;

import java.util.Collection;
import java.util.HashSet;
import java.util.Optional;

public class UninitializedVariableDetector implements ASTVisitor<Boolean, Boolean> {
    private final ErrorReporter errorReporter;
    private SymbolTable<Object> symbolTable;
    private final Collection<String> uninitializedIdentifiers;

    public UninitializedVariableDetector(ErrorReporter errorReporter) {
        this.errorReporter = errorReporter;
        symbolTable = new SymbolTable<>();
        uninitializedIdentifiers = new HashSet<>();
    }

    @Override
    public Boolean visitAdd(AddExpression addExpression) {
        return addExpression.getLeftExpression().accept(this) &&
                addExpression.getRightExpression().accept(this);
    }

    @Override
    public Boolean visitAssignment(AssignmentExpression assignmentExpression) {
        boolean result = assignmentExpression.getExpression().accept(this);
        symbolTable.put(assignmentExpression.getId(), null);
        return result;
    }

    @Override
    public Boolean visitConstant(Constant constant) {
        return true;
    }

    @Override
    public Boolean visitDiv(DivExpression divExpression) {
        return divExpression.getLeftExpression().accept(this) &&
                divExpression.getRightExpression().accept(this);
    }

    @Override
    public Boolean visitInput(InputExpression inputExpression) {
        return true;
    }

    @Override
    public Boolean visitMod(ModExpression modExpression) {
        return modExpression.getLeftExpression().accept(this) &&
                modExpression.getRightExpression().accept(this);
    }

    @Override
    public Boolean visitMult(MultExpression multExpression) {
        return multExpression.getLeftExpression().accept(this) &&
                multExpression.getRightExpression().accept(this);
    }

    @Override
    public Boolean visitNegative(NegativeExpression negativeExpression) {
        return negativeExpression.getExpression().accept(this);
    }

    @Override
    public Boolean visitSub(SubExpression subExpression) {
        return subExpression.getLeftExpression().accept(this) &&
                subExpression.getRightExpression().accept(this);
    }

    @Override
    public Boolean visitTernary(TernaryExpression ternaryExpression) {
        boolean result = ternaryExpression.getCondition().accept(this);

        SymbolTable<Object> trueExpressionScope = symbolTable.pushScope();
        SymbolTable<Object> falseExpressionScope = symbolTable.pushScope();

        this.symbolTable = trueExpressionScope;
        result &= ternaryExpression.getTrueExpression().accept(this);

        this.symbolTable = falseExpressionScope;
        result &= ternaryExpression.getFalseExpression().accept(this);

        this.symbolTable = trueExpressionScope.intersectInnerScope(falseExpressionScope);

        return result;
    }

    @Override
    public Boolean visitVariable(Variable variable) {
        if (!symbolTable.contains(variable.getId())) {
            System.err.println(errorReporter.getWarningMessage("uninitialized variable '" + variable.getId() + "'",
                    variable.getStartLocation()));
            return false;
        }

        return true;
    }

    @Override
    public Boolean visitAssignment(AssignmentInstruction assignmentInstruction) {
        boolean result = assignmentInstruction.getExpression().accept(this);
        symbolTable.put(assignmentInstruction.getId(), null);
        return result;
    }

    @Override
    public Boolean visitEmpty(EmptyInstruction emptyInstruction) {
        return true;
    }

    @Override
    public Boolean visitLoop(LoopInstruction loopInstruction) {
        boolean result = loopInstruction.getCondition().accept(this);
        this.symbolTable = symbolTable.pushScope();
        for (Instruction instruction : loopInstruction.getInstructionList())
            result &= instruction.accept(this);

        this.symbolTable = symbolTable.popScope();
        return result;
    }

    @Override
    public Boolean visitOutput(OutputInstruction outputInstruction) {
        Optional<Expression> expression = outputInstruction.getExpression();
        if (expression.isPresent())
            return expression.get().accept(this);

        return true;
    }

    @Override
    public Boolean visitProgram(Program program) {
        return program
                .getInstructionList()
                .stream()
                .allMatch(i -> i.accept(this));
    }
}
