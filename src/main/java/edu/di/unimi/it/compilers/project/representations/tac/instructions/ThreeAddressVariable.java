package edu.di.unimi.it.compilers.project.representations.tac.instructions;

import java.util.Objects;

public class ThreeAddressVariable implements ThreeAddressOperand {
    private final String identifier;

    public ThreeAddressVariable(String identifier) {
        this.identifier = identifier;
    }

    public String getIdentifier() {
        return identifier;
    }

    @Override
    public String toString() {
        return identifier;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ThreeAddressVariable that = (ThreeAddressVariable) o;
        return Objects.equals(identifier, that.identifier);
    }

    @Override
    public int hashCode() {
        return Objects.hash(identifier);
    }
}
