package edu.di.unimi.it.compilers.project.compilation.ast;

import edu.di.unimi.it.compilers.project.parsing.ast.ASTVisitor;
import edu.di.unimi.it.compilers.project.parsing.ast.expressions.*;
import edu.di.unimi.it.compilers.project.parsing.ast.instructions.*;
import edu.di.unimi.it.compilers.project.utils.SymbolTable;
import lt.macchina.Codice;
import lt.macchina.Macchina;

import java.util.Optional;

public class ASTVariableAllocator implements ASTVisitor<Codice, Codice> {
    public static final int VARIABLE_SIZE = 1;

    private int currentAddress;
    private final SymbolTable<Integer> symbolTable;
    private final Codice generatedCode;

    public ASTVariableAllocator(Codice generatedCode) {
        this.symbolTable = new SymbolTable<>();
        this.generatedCode = generatedCode;
    }

    @Override
    public Codice visitAdd(AddExpression addExpression) {
        addExpression.getLeftExpression().accept(this);
        return addExpression.getRightExpression().accept(this);
    }

    @Override
    public Codice visitAssignment(AssignmentExpression assignmentExpression) {
        assignmentExpression.getExpression().accept(this);
        allocateVariable(assignmentExpression.getId(), VARIABLE_SIZE);
        return generatedCode;
    }

    @Override
    public Codice visitConstant(Constant constant) {
        return generatedCode;
    }

    @Override
    public Codice visitDiv(DivExpression divExpression) {
        divExpression.getLeftExpression().accept(this);
        return divExpression.getRightExpression().accept(this);
    }

    @Override
    public Codice visitInput(InputExpression inputExpression) {
        return generatedCode;
    }

    @Override
    public Codice visitMod(ModExpression modExpression) {
        modExpression.getLeftExpression().accept(this);
        return modExpression.getRightExpression().accept(this);
    }

    @Override
    public Codice visitMult(MultExpression multExpression) {
        multExpression.getLeftExpression().accept(this);
        return multExpression.getRightExpression().accept(this);
    }

    @Override
    public Codice visitNegative(NegativeExpression negativeExpression) {
        return negativeExpression.getExpression().accept(this);
    }

    @Override
    public Codice visitSub(SubExpression subExpression) {
        subExpression.getLeftExpression().accept(this);
        return subExpression.getRightExpression().accept(this);
    }

    @Override
    public Codice visitTernary(TernaryExpression ternaryExpression) {
        ternaryExpression.getCondition().accept(this);
        ternaryExpression.getTrueExpression().accept(this);
        return ternaryExpression.getFalseExpression().accept(this);
    }

    @Override
    public Codice visitVariable(Variable variable) {
        allocateVariable(variable.getId(), VARIABLE_SIZE);
        return generatedCode;
    }

    @Override
    public Codice visitAssignment(AssignmentInstruction assignmentInstruction) {
        assignmentInstruction.getExpression().accept(this);
        allocateVariable(assignmentInstruction.getId(), VARIABLE_SIZE);
        return generatedCode;
    }

    @Override
    public Codice visitEmpty(EmptyInstruction emptyInstruction) {
        return generatedCode;
    }

    @Override
    public Codice visitLoop(LoopInstruction loopInstruction) {
        loopInstruction.getCondition().accept(this);
        for (Instruction instruction : loopInstruction.getInstructionList())
            instruction.accept(this);

        return generatedCode;
    }

    @Override
    public Codice visitOutput(OutputInstruction outputInstruction) {
        Optional<Expression> expression = outputInstruction.getExpression();
        if (expression.isPresent())
            return expression.get().accept(this);

        return generatedCode;
    }

    @Override
    public Codice visitProgram(Program program) {
        for (Instruction instruction : program.getInstructionList())
            instruction.accept(this);
        return generatedCode;
    }

    public void allocateVariable(String id, int variableSize) {
        if (symbolTable.contains(id))
            return;

        symbolTable.put(id, currentAddress);
        currentAddress += variableSize;
        generatedCode.genera(Macchina.PUSHIMM, 0);
    }

    public boolean contains(String identifier) {
        return this.symbolTable.contains(identifier);
    }

    public Integer getLocation(String identifier) {
        return this.symbolTable.get(identifier);
    }
}
