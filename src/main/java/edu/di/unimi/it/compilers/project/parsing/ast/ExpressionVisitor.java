package edu.di.unimi.it.compilers.project.parsing.ast;

import edu.di.unimi.it.compilers.project.parsing.ast.expressions.*;

public interface ExpressionVisitor<T> {
    T visitAdd(AddExpression addExpression);

    T visitAssignment(AssignmentExpression assignmentExpression);

    T visitConstant(Constant constant);

    T visitDiv(DivExpression divExpression);

    T visitInput(InputExpression inputExpression);

    T visitMod(ModExpression modExpression);

    T visitMult(MultExpression multExpression);

    T visitNegative(NegativeExpression negativeExpression);

    T visitSub(SubExpression subExpression);

    T visitTernary(TernaryExpression ternaryExpression);

    T visitVariable(Variable variable);
}
