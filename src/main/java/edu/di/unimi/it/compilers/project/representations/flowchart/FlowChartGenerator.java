package edu.di.unimi.it.compilers.project.representations.flowchart;

import edu.di.unimi.it.compilers.project.representations.flowchart.basicblocks.BasicBlock;
import edu.di.unimi.it.compilers.project.representations.flowchart.basicblocks.BasicBlocksProgram;
import edu.di.unimi.it.compilers.project.representations.tac.ThreeAddressCodeVisitor;
import edu.di.unimi.it.compilers.project.representations.tac.instructions.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class FlowChartGenerator implements ThreeAddressCodeVisitor<BasicBlocksProgram<ThreeAddressInstruction>> {
    private final List<Integer> leaders;
    private int instructionIndex;

    public FlowChartGenerator() {
        leaders = new ArrayList<>();
    }

    @Override
    public BasicBlocksProgram<ThreeAddressInstruction> visitAdd(AddInstruction addInstruction) {
        return null;
    }

    @Override
    public BasicBlocksProgram<ThreeAddressInstruction> visitCopy(CopyInstruction copyInstruction) {
        return null;
    }

    @Override
    public BasicBlocksProgram<ThreeAddressInstruction> visitDiv(DivInstruction divInstruction) {
        return null;
    }

    @Override
    public BasicBlocksProgram<ThreeAddressInstruction> visitHalt(HaltInstruction haltInstruction) {
        return null;
    }

    @Override
    public BasicBlocksProgram<ThreeAddressInstruction> visitInput(InputInstruction inputInstruction) {
        return null;
    }

    @Override
    public BasicBlocksProgram<ThreeAddressInstruction> visitJump(JumpInstruction jumpInstruction) {
        leaders.add(jumpInstruction.getAddress());
        leaders.add(instructionIndex + 1);
        return null;
    }

    @Override
    public BasicBlocksProgram<ThreeAddressInstruction> visitJumpZero(JumpZeroInstruction jumpZeroInstruction) {
        leaders.add(jumpZeroInstruction.getAddress());
        leaders.add(instructionIndex + 1);
        return null;
    }

    @Override
    public BasicBlocksProgram<ThreeAddressInstruction> visitMult(MultInstruction multInstruction) {
        return null;
    }

    @Override
    public BasicBlocksProgram<ThreeAddressInstruction> visitOutput(OutputInstruction outputInstruction) {
        return null;
    }

    @Override
    public BasicBlocksProgram<ThreeAddressInstruction> visitSub(SubInstruction subInstruction) {
        return null;
    }

    @Override
    public BasicBlocksProgram<ThreeAddressInstruction> visitProgram(ThreeAddressProgram program) {
        List<ThreeAddressInstruction> instructions = program.getInstructionList();

        markLeaders(instructions);
        List<BasicBlock<ThreeAddressInstruction>> basicBlocks = generateBasicBlocks(instructions);

        BasicBlocksProgram<ThreeAddressInstruction> basicBlocksProgram = new BasicBlocksProgram<>(basicBlocks);

        System.out.println(basicBlocksProgram);
        generateFlowGraph(basicBlocksProgram);

        return basicBlocksProgram;
    }

    private void generateFlowGraph(BasicBlocksProgram<ThreeAddressInstruction> basicBlocksProgram) {
        List<BasicBlock<ThreeAddressInstruction>> basicBlocks = basicBlocksProgram.getBasicBlocks();
        for (BasicBlock<ThreeAddressInstruction> block : basicBlocks.subList(0, basicBlocks.size() - 2)) {
            int blockLength = block.getInstructionList().size();
            ThreeAddressInstruction lastInstruction = block.getInstructionList().get(blockLength - 1);
            if (lastInstruction instanceof JumpZeroInstruction) {
                BasicBlock successor = basicBlocksProgram.getBlockForInstruction(block.getBlockEnd());
                block.addSuccessor(successor);
                successor.addPredecessor(block);

                successor = basicBlocksProgram
                        .getBlockForInstruction(((JumpZeroInstruction) lastInstruction).getAddress());
                block.addSuccessor(successor);
                successor.addPredecessor(block);
            } else if (lastInstruction instanceof JumpInstruction) {
                BasicBlock successor = basicBlocksProgram
                        .getBlockForInstruction(((JumpInstruction) lastInstruction).getAddress());
                block.addSuccessor(successor);
                successor.addPredecessor(block);
            } else {
                BasicBlock successor = basicBlocksProgram.getBlockForInstruction(block.getBlockEnd());
                block.addSuccessor(successor);
                successor.addPredecessor(block);
            }
        }
    }

    private void markLeaders(List<ThreeAddressInstruction> instructions) {
        leaders.add(0);
        for (ThreeAddressInstruction instruction : instructions) {
            instruction.accept(this);
            instructionIndex++;
        }
        leaders.add(instructionIndex);
    }

    private List<BasicBlock<ThreeAddressInstruction>> generateBasicBlocks(List<ThreeAddressInstruction> instructions) {
        List<BasicBlock<ThreeAddressInstruction>> basicBlocks = new ArrayList<>();
        Collections.sort(leaders);
        for (int i = 0; i < leaders.size() - 1; i++) {
            int blockStart = leaders.get(i);
            int blockEnd = leaders.get(i + 1);
            if (blockStart != blockEnd)
                basicBlocks.add(new BasicBlock<>(instructions.subList(blockStart, blockEnd), blockStart, blockEnd));
        }
        return basicBlocks;
    }
}
