package edu.di.unimi.it.compilers.project.parsing.ast.expressions;

import edu.di.unimi.it.compilers.project.parsing.ast.ASTNode;
import edu.di.unimi.it.compilers.project.parsing.ast.ExpressionVisitor;

public interface Expression extends ASTNode {
    <T> T accept(ExpressionVisitor<T> expressionVisitor);
}
