package edu.di.unimi.it.compilers.project.representations.tac.instructions;

import edu.di.unimi.it.compilers.project.representations.tac.ThreeAddressCodeVisitor;

import java.util.Objects;
import java.util.Optional;

public class OutputInstruction implements ThreeAddressInstruction {
    private final Optional<String> string;
    private final Optional<ThreeAddressOperand> expression;

    public OutputInstruction(Optional<String> string, Optional<ThreeAddressOperand> expression) {
        this.string = string;
        this.expression = expression;
    }

    public Optional<String> getString() {
        return string;
    }

    public Optional<ThreeAddressOperand> getExpression() {
        return expression;
    }

    @Override
    public <T> T accept(ThreeAddressCodeVisitor<T> visitor) {
        return visitor.visitOutput(this);
    }

    @Override
    public String toString() {
        return "output" + (string.isPresent() ? " \"" + string.get() + "\"" : "") +
                (expression.isPresent() ? " " + expression.get() : "");
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        OutputInstruction that = (OutputInstruction) o;
        return Objects.equals(string, that.string) &&
                Objects.equals(expression, that.expression);
    }

    @Override
    public int hashCode() {
        return Objects.hash(string, expression);
    }
}
