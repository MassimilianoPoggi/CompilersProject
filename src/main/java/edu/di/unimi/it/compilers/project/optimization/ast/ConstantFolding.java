package edu.di.unimi.it.compilers.project.optimization.ast;

import edu.di.unimi.it.compilers.project.parsing.ast.ASTVisitor;
import edu.di.unimi.it.compilers.project.parsing.ast.expressions.*;
import edu.di.unimi.it.compilers.project.parsing.ast.instructions.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class ConstantFolding implements ASTVisitor<Expression, Instruction> {
    @Override
    public Expression visitAdd(AddExpression addExpression) {
        Expression leftExpression = addExpression.getLeftExpression().accept(this);
        Expression rightExpression = addExpression.getRightExpression().accept(this);

        if (leftExpression instanceof Constant && rightExpression instanceof Constant) {
            int value = ((Constant) leftExpression).getValue() + ((Constant) rightExpression).getValue();
            return new Constant(value);
        }

        return new AddExpression(leftExpression, rightExpression);
    }

    @Override
    public Expression visitAssignment(AssignmentExpression assignmentExpression) {
        Expression expression = assignmentExpression.getExpression().accept(this);
        String identifier = assignmentExpression.getId();
        return new AssignmentExpression(identifier, expression);
    }

    @Override
    public Expression visitConstant(Constant constant) {
        return constant;
    }

    @Override
    public Expression visitDiv(DivExpression divExpression) {
        Expression leftExpression = divExpression.getLeftExpression().accept(this);
        Expression rightExpression = divExpression.getRightExpression().accept(this);

        if (leftExpression instanceof Constant && rightExpression instanceof Constant) {
            int value = ((Constant) leftExpression).getValue() / ((Constant) rightExpression).getValue();
            return new Constant(value);
        }

        return new DivExpression(leftExpression, rightExpression);
    }

    @Override
    public Expression visitInput(InputExpression inputExpression) {
        return inputExpression;
    }

    @Override
    public Expression visitMod(ModExpression modExpression) {
        Expression leftExpression = modExpression.getLeftExpression().accept(this);
        Expression rightExpression = modExpression.getRightExpression().accept(this);

        if (leftExpression instanceof Constant && rightExpression instanceof Constant) {
            int value = Math.floorMod(((Constant) leftExpression).getValue(), ((Constant) rightExpression).getValue());
            return new Constant(value);
        }

        return new ModExpression(leftExpression, rightExpression);
    }

    @Override
    public Expression visitMult(MultExpression multExpression) {
        Expression leftExpression = multExpression.getLeftExpression().accept(this);
        Expression rightExpression = multExpression.getRightExpression().accept(this);

        if (leftExpression instanceof Constant && rightExpression instanceof Constant) {
            int value = ((Constant) leftExpression).getValue() * ((Constant) rightExpression).getValue();
            return new Constant(value);
        }

        return new MultExpression(leftExpression, rightExpression);
    }

    @Override
    public Expression visitNegative(NegativeExpression negativeExpression) {
        Expression expression = negativeExpression.getExpression().accept(this);
        if (expression instanceof Constant)
            return new Constant(-((Constant) expression).getValue());

        return new NegativeExpression(expression);
    }

    @Override
    public Expression visitSub(SubExpression subExpression) {
        Expression leftExpression = subExpression.getLeftExpression().accept(this);
        Expression rightExpression = subExpression.getRightExpression().accept(this);

        if (leftExpression instanceof Constant && rightExpression instanceof Constant) {
            int value = ((Constant) leftExpression).getValue() - ((Constant) rightExpression).getValue();
            return new Constant(value);
        }

        return new SubExpression(leftExpression, rightExpression);
    }

    @Override
    public Expression visitTernary(TernaryExpression ternaryExpression) {
        Expression condition = ternaryExpression.getCondition().accept(this);
        Expression trueExpression = ternaryExpression.getTrueExpression().accept(this);
        Expression falseExpression = ternaryExpression.getFalseExpression().accept(this);
        return new TernaryExpression(condition, trueExpression, falseExpression);
    }

    @Override
    public Expression visitVariable(Variable variable) {
        return variable;
    }

    @Override
    public Instruction visitAssignment(AssignmentInstruction assignmentInstruction) {
        Expression expression = assignmentInstruction.getExpression().accept(this);
        String identifier = assignmentInstruction.getId();
        return new AssignmentInstruction(identifier, expression);
    }

    @Override
    public Instruction visitEmpty(EmptyInstruction emptyInstruction) {
        return emptyInstruction;
    }

    @Override
    public Instruction visitLoop(LoopInstruction loopInstruction) {
        Expression condition = loopInstruction.getCondition().accept(this);
        List<Instruction> instructions = new ArrayList<>();
        for (Instruction instruction : loopInstruction.getInstructionList())
            instructions.add(instruction.accept(this));
        return new LoopInstruction(condition, instructions);
    }

    @Override
    public Instruction visitOutput(OutputInstruction outputInstruction) {
        Optional<Expression> expression = outputInstruction.getExpression();
        if (expression.isPresent()) {
            Expression simplifiedExpression = expression.get().accept(this);
            return new OutputInstruction(outputInstruction.getString(), Optional.of(simplifiedExpression));
        }
        return outputInstruction;
    }

    @Override
    public Instruction visitProgram(Program program) {
        List<Instruction> instructions = new ArrayList<>();
        for (Instruction instruction : program.getInstructionList())
            instructions.add(instruction.accept(this));

        return new Program(instructions);
    }
}