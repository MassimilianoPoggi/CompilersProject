package edu.di.unimi.it.compilers.project.representations.tac.instructions;

import edu.di.unimi.it.compilers.project.representations.tac.ThreeAddressCodeVisitor;

import java.util.Objects;
import java.util.Optional;

public class InputInstruction implements ThreeAddressInstruction {
    private final ThreeAddressVariable result;
    private final Optional<String> string;

    public InputInstruction(ThreeAddressVariable result, Optional<String> string) {
        this.result = result;
        this.string = string;
    }

    public ThreeAddressVariable getResult() {
        return result;
    }

    public Optional<String> getString() {
        return string;
    }

    @Override
    public <T> T accept(ThreeAddressCodeVisitor<T> visitor) {
        return visitor.visitInput(this);
    }

    @Override
    public String toString() {
        return result + " = input" + (string.isPresent() ? " \"" + string.get() + "\"" : "");
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        InputInstruction that = (InputInstruction) o;
        return Objects.equals(result, that.result) &&
                Objects.equals(string, that.string);
    }

    @Override
    public int hashCode() {
        return Objects.hash(result, string);
    }
}
