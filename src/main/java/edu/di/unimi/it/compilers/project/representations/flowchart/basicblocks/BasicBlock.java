package edu.di.unimi.it.compilers.project.representations.flowchart.basicblocks;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class BasicBlock<T> {
    private final List<T> instructionList;

    private final int blockStart;
    private final int blockEnd;

    private final List<BasicBlock<T>> successors;
    private final List<BasicBlock<T>> predecessors;

    public BasicBlock(List<T> instructionList, int blockStart, int blockEnd) {
        this.instructionList = instructionList;

        this.blockStart = blockStart;
        this.blockEnd = blockEnd;

        successors = new ArrayList<>();
        predecessors = new ArrayList<>();
    }

    public List<T> getInstructionList() {
        return instructionList;
    }

    public int getBlockStart() {
        return blockStart;
    }

    public int getBlockEnd() {
        return blockEnd;
    }

    public boolean containsInstruction(int instructionIndex) {
        return blockStart <= instructionIndex && instructionIndex < blockEnd;
    }

    public void addSuccessor(BasicBlock<T> successor) {
        this.successors.add(successor);
    }

    public void addPredecessor(BasicBlock<T> predecessor) {
        this.predecessors.add(predecessor);
    }

    public List<BasicBlock<T>> getSuccessors() {
        return successors;
    }

    public List<BasicBlock<T>> getPredecessors() {
        return predecessors;
    }

    @Override
    public String toString() {
        return "Block starting at " + blockStart + " and ending at " + blockEnd + "\n" +
                instructionList
                        .stream()
                        .map(Object::toString)
                        .collect(Collectors.joining("\n"));
    }
}
