package edu.di.unimi.it.compilers.project.parsing.ast;

public interface ASTVisitor<T, U> extends ExpressionVisitor<T>, InstructionVisitor<U> {
}
