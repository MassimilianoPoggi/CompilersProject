package edu.di.unimi.it.compilers.project.parsing.ast.instructions;

import edu.di.unimi.it.compilers.project.parsing.ast.ASTNode;
import edu.di.unimi.it.compilers.project.parsing.ast.InstructionVisitor;

public interface Instruction extends ASTNode {
    <T> T accept(InstructionVisitor<T> instructionVisitor);
}
