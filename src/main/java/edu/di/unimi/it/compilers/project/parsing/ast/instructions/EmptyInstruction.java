package edu.di.unimi.it.compilers.project.parsing.ast.instructions;

import edu.di.unimi.it.compilers.project.parsing.ast.InstructionVisitor;

public class EmptyInstruction implements Instruction {
    @Override
    public <T> T accept(InstructionVisitor<T> instructionVisitor) {
        return instructionVisitor.visitEmpty(this);
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof EmptyInstruction;
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}
