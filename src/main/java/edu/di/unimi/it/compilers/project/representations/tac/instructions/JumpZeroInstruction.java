package edu.di.unimi.it.compilers.project.representations.tac.instructions;

import edu.di.unimi.it.compilers.project.representations.tac.ThreeAddressCodeVisitor;

import java.util.Objects;

public class JumpZeroInstruction extends JumpInstruction {
    private final ThreeAddressOperand condition;

    public JumpZeroInstruction(ThreeAddressOperand condition) {
        this(condition, 0);
    }

    public JumpZeroInstruction(ThreeAddressOperand condition, int address) {
        super(address);
        this.condition = condition;
    }

    public ThreeAddressOperand getCondition() {
        return condition;
    }

    @Override
    public <T> T accept(ThreeAddressCodeVisitor<T> visitor) {
        return visitor.visitJumpZero(this);
    }

    @Override
    public String toString() {
        return "if " + condition + " == 0 " + super.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        JumpZeroInstruction that = (JumpZeroInstruction) o;
        return Objects.equals(condition, that.condition);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), condition);
    }
}
