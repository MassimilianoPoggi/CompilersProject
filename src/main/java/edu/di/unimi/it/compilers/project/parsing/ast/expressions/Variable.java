package edu.di.unimi.it.compilers.project.parsing.ast.expressions;

import edu.di.unimi.it.compilers.project.parsing.ast.ExpressionVisitor;
import java_cup.runtime.ComplexSymbolFactory;

import java.util.Objects;

public class Variable implements Expression {
    private final String id;
    private final ComplexSymbolFactory.Location startLocation;

    public Variable(String id, ComplexSymbolFactory.Location startLocation) {
        this.id = id;
        this.startLocation = startLocation;
    }

    public String getId() {
        return id;
    }

    @Override
    public <T> T accept(ExpressionVisitor<T> expressionVisitor) {
        return expressionVisitor.visitVariable(this);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Variable variable = (Variable) o;
        return Objects.equals(id, variable.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    public ComplexSymbolFactory.Location getStartLocation() {
        return startLocation;
    }
}
