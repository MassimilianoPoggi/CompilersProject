package edu.di.unimi.it.compilers.project.representations.tac.instructions;

import edu.di.unimi.it.compilers.project.representations.tac.ThreeAddressCodeVisitor;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ThreeAddressProgram {
    private final List<ThreeAddressInstruction> instructionList;

    public ThreeAddressProgram() {
        instructionList = new ArrayList<>();
    }

    public int addInstruction(ThreeAddressInstruction instruction) {
        instructionList.add(instruction);
        return instructionList.size() - 1;
    }

    public int getNextInstructionAddress() {
        return instructionList.size();
    }

    public List<ThreeAddressInstruction> getInstructionList() {
        return Collections.unmodifiableList(instructionList);
    }

    public <T> T accept(ThreeAddressCodeVisitor<T> visitor) {
        return visitor.visitProgram(this);
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < instructionList.size(); i++) {
            builder.append(i);
            builder.append(": ");
            builder.append(instructionList.get(i));
            builder.append("\n");
        }
        return builder.toString();
    }
}
