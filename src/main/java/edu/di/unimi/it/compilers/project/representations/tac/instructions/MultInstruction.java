package edu.di.unimi.it.compilers.project.representations.tac.instructions;

import edu.di.unimi.it.compilers.project.representations.tac.ThreeAddressCodeVisitor;

import java.util.Objects;

public class MultInstruction implements ThreeAddressInstruction {
    private final ThreeAddressVariable result;
    private final ThreeAddressOperand left;
    private final ThreeAddressOperand right;

    public MultInstruction(ThreeAddressVariable result, ThreeAddressOperand left, ThreeAddressOperand right) {
        this.result = result;
        this.left = left;
        this.right = right;
    }

    public ThreeAddressVariable getResult() {
        return result;
    }

    public ThreeAddressOperand getLeft() {
        return left;
    }

    public ThreeAddressOperand getRight() {
        return right;
    }

    @Override
    public <T> T accept(ThreeAddressCodeVisitor<T> visitor) {
        return visitor.visitMult(this);
    }

    @Override
    public String toString() {
        return result + " = " + left + " * " + right;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MultInstruction that = (MultInstruction) o;
        return Objects.equals(result, that.result) &&
                Objects.equals(left, that.left) &&
                Objects.equals(right, that.right);
    }

    @Override
    public int hashCode() {
        return Objects.hash(result, left, right);
    }
}
