package edu.di.unimi.it.compilers.project.parsing.ast.instructions;

import edu.di.unimi.it.compilers.project.parsing.ast.InstructionVisitor;
import edu.di.unimi.it.compilers.project.parsing.ast.expressions.Expression;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

public class LoopInstruction implements Instruction {
    private final Expression condition;
    private final List<Instruction> instructionList;

    public LoopInstruction(Expression condition, List<Instruction> instructionList) {
        this.condition = condition;
        this.instructionList = instructionList;
    }

    public Expression getCondition() {
        return condition;
    }

    public List<Instruction> getInstructionList() {
        return Collections.unmodifiableList(instructionList);
    }

    @Override
    public <T> T accept(InstructionVisitor<T> instructionVisitor) {
        return instructionVisitor.visitLoop(this);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LoopInstruction that = (LoopInstruction) o;
        return Objects.equals(condition, that.condition) &&
                Objects.equals(instructionList, that.instructionList);
    }

    @Override
    public int hashCode() {
        return Objects.hash(condition, instructionList);
    }
}
