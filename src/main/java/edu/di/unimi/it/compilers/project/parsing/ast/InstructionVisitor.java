package edu.di.unimi.it.compilers.project.parsing.ast;

import edu.di.unimi.it.compilers.project.parsing.ast.instructions.*;

public interface InstructionVisitor<T> {
    T visitAssignment(AssignmentInstruction assignmentInstruction);

    T visitEmpty(EmptyInstruction emptyInstruction);

    T visitLoop(LoopInstruction loopInstruction);

    T visitOutput(OutputInstruction outputInstruction);

    T visitProgram(Program program);
}
