package edu.di.unimi.it.compilers.project.parsing.ast.instructions;

import edu.di.unimi.it.compilers.project.parsing.ast.InstructionVisitor;
import edu.di.unimi.it.compilers.project.parsing.ast.expressions.Expression;

import java.util.Objects;

public class AssignmentInstruction implements Instruction {
    private final String id;
    private final Expression expression;

    public AssignmentInstruction(String id, Expression expression) {
        this.id = id;
        this.expression = expression;
    }

    public String getId() {
        return id;
    }

    public Expression getExpression() {
        return expression;
    }

    @Override
    public <T> T accept(InstructionVisitor<T> instructionVisitor) {
        return instructionVisitor.visitAssignment(this);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AssignmentInstruction that = (AssignmentInstruction) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(expression, that.expression);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, expression);
    }
}
