package edu.di.unimi.it.compilers.project.compilation.tac;

import edu.di.unimi.it.compilers.project.representations.tac.ThreeAddressCodeVisitor;
import edu.di.unimi.it.compilers.project.representations.tac.instructions.*;
import lt.macchina.Codice;
import lt.macchina.Macchina;

import java.util.*;

public class TACToMachineCompiler implements ThreeAddressCodeVisitor<Codice> {
    private final Codice generatedCode;
    private final TACVariableAllocator tacVariableAllocator;
    private final List<Integer> indexToJumpAddress;
    private final Map<Integer, List<Integer>> emptyJumpAddresses;

    public TACToMachineCompiler(Codice generatedCode, TACVariableAllocator tacVariableAllocator) {
        this.generatedCode = generatedCode;
        this.tacVariableAllocator = tacVariableAllocator;
        indexToJumpAddress = new ArrayList<>();
        emptyJumpAddresses = new HashMap<>();
    }

    @Override
    public Codice visitAdd(AddInstruction addInstruction) {
        pushOperand(addInstruction.getLeft());
        pushOperand(addInstruction.getRight());
        generatedCode.genera(Macchina.ADD);
        popResult(addInstruction.getResult());
        return generatedCode;
    }

    @Override
    public Codice visitCopy(CopyInstruction copyInstruction) {
        pushOperand(copyInstruction.getExpression());
        popResult(copyInstruction.getResult());
        return generatedCode;
    }

    @Override
    public Codice visitDiv(DivInstruction divInstruction) {
        pushOperand(divInstruction.getLeft());
        pushOperand(divInstruction.getRight());
        generatedCode.genera(Macchina.DIV);
        popResult(divInstruction.getResult());
        return generatedCode;
    }

    @Override
    public Codice visitHalt(HaltInstruction haltInstruction) {
        generatedCode.genera(Macchina.HALT);
        return generatedCode;
    }

    @Override
    public Codice visitInput(InputInstruction inputInstruction) {
        printString(inputInstruction.getString());
        generatedCode.genera(Macchina.INPUT);
        popResult(inputInstruction.getResult());
        return generatedCode;
    }

    @Override
    public Codice visitJump(JumpInstruction jumpInstruction) {
        int jumpSlot = generatedCode.generaParziale(Macchina.JUMP);
        addSlotForJumpAddress(jumpInstruction, jumpSlot);
        return generatedCode;
    }

    @Override
    public Codice visitJumpZero(JumpZeroInstruction jumpZeroInstruction) {
        pushOperand(jumpZeroInstruction.getCondition());
        int jumpSlot = generatedCode.generaParziale(Macchina.JZERO);
        addSlotForJumpAddress(jumpZeroInstruction, jumpSlot);
        return generatedCode;
    }

    @Override
    public Codice visitMult(MultInstruction multInstruction) {
        pushOperand(multInstruction.getLeft());
        pushOperand(multInstruction.getRight());
        generatedCode.genera(Macchina.MUL);
        popResult(multInstruction.getResult());
        return generatedCode;
    }

    @Override
    public Codice visitOutput(OutputInstruction outputInstruction) {
        printString(outputInstruction.getString());
        Optional<ThreeAddressOperand> expression = outputInstruction.getExpression();
        if (expression.isPresent()) {
            pushOperand(expression.get());
            generatedCode.genera(Macchina.OUTPUT);
        }
        return null;
    }

    @Override
    public Codice visitSub(SubInstruction subInstruction) {
        pushOperand(subInstruction.getLeft());
        pushOperand(subInstruction.getRight());
        generatedCode.genera(Macchina.SUB);
        popResult(subInstruction.getResult());
        return generatedCode;
    }

    @Override
    public Codice visitProgram(ThreeAddressProgram program) {
        tacVariableAllocator.visitProgram(program);
        for (ThreeAddressInstruction instruction : program.getInstructionList()) {
            indexToJumpAddress.add(generatedCode.indirizzoProssimaIstruzione());
            instruction.accept(this);
        }
        fillJumpAddresses(program);
        return generatedCode;
    }

    private void fillJumpAddresses(ThreeAddressProgram program) {
        for (Map.Entry<Integer, List<Integer>> emptySlots : emptyJumpAddresses.entrySet()) {
            int jumpAddress = indexToJumpAddress.get(emptySlots.getKey());
            for (Integer emptySlot : emptySlots.getValue())
                generatedCode.completaIstruzione(emptySlot, jumpAddress);
        }
    }

    private void addSlotForJumpAddress(JumpInstruction jumpInstruction, int slot) {
        Integer address = jumpInstruction.getAddress();
        List<Integer> partialSlotsForInstruction = emptyJumpAddresses.getOrDefault(address, new ArrayList<>());
        partialSlotsForInstruction.add(slot);
        emptyJumpAddresses.putIfAbsent(address, partialSlotsForInstruction);
    }

    private void pushOperand(ThreeAddressOperand operand) {
        if (operand instanceof ThreeAddressConstant)
            generatedCode.genera(Macchina.PUSHIMM, ((ThreeAddressConstant) operand).getValue());
        else
            generatedCode.genera(Macchina.PUSH,
                    tacVariableAllocator.getLocation(((ThreeAddressVariable) operand).getIdentifier()));
    }

    private void popResult(ThreeAddressVariable result) {
        generatedCode.genera(Macchina.POP, tacVariableAllocator.getLocation(result.getIdentifier()));
    }

    private void printString(Optional<String> message) {
        if (message.isPresent()) {
            for (char c : message.get().toCharArray()) {
                generatedCode.genera(Macchina.PUSHIMM, c);
                generatedCode.genera(Macchina.OUTPUTCH);
            }
        }
    }
}
