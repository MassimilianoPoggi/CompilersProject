package edu.di.unimi.it.compilers.project.compilation.tac;

import edu.di.unimi.it.compilers.project.representations.tac.ThreeAddressCodeVisitor;
import edu.di.unimi.it.compilers.project.representations.tac.instructions.*;
import edu.di.unimi.it.compilers.project.utils.SymbolTable;
import lt.macchina.Codice;
import lt.macchina.Macchina;

public class TACVariableAllocator implements ThreeAddressCodeVisitor<Codice> {
    public static final int VARIABLE_SIZE = 1;

    private int currentAddress;
    private final SymbolTable<Integer> symbolTable;
    private final Codice generatedCode;

    public TACVariableAllocator(Codice generatedCode) {
        this.generatedCode = generatedCode;
        symbolTable = new SymbolTable<>();
        currentAddress = 0;
    }

    @Override
    public Codice visitAdd(AddInstruction addInstruction) {
        allocateVariable(addInstruction.getResult().getIdentifier(), VARIABLE_SIZE);
        return generatedCode;
    }

    @Override
    public Codice visitCopy(CopyInstruction copyInstruction) {
        allocateVariable(copyInstruction.getResult().getIdentifier(), VARIABLE_SIZE);
        return generatedCode;
    }

    @Override
    public Codice visitDiv(DivInstruction divInstruction) {
        allocateVariable(divInstruction.getResult().getIdentifier(), VARIABLE_SIZE);
        return generatedCode;
    }

    @Override
    public Codice visitHalt(HaltInstruction haltInstruction) {
        return generatedCode;
    }

    @Override
    public Codice visitInput(InputInstruction inputInstruction) {
        allocateVariable(inputInstruction.getResult().getIdentifier(), VARIABLE_SIZE);
        return generatedCode;
    }

    @Override
    public Codice visitJump(JumpInstruction jumpInstruction) {
        return generatedCode;
    }

    @Override
    public Codice visitJumpZero(JumpZeroInstruction jumpZeroInstruction) {
        return generatedCode;
    }

    @Override
    public Codice visitMult(MultInstruction multInstruction) {
        allocateVariable(multInstruction.getResult().getIdentifier(), VARIABLE_SIZE);
        return generatedCode;
    }

    @Override
    public Codice visitOutput(OutputInstruction outputInstruction) {
        return generatedCode;
    }

    @Override
    public Codice visitSub(SubInstruction subInstruction) {
        allocateVariable(subInstruction.getResult().getIdentifier(), VARIABLE_SIZE);
        return generatedCode;
    }

    @Override
    public Codice visitProgram(ThreeAddressProgram program) {
        for (ThreeAddressInstruction instruction : program.getInstructionList())
            instruction.accept(this);

        return generatedCode;
    }

    public void allocateVariable(String id, int variableSize) {
        if (symbolTable.contains(id))
            return;

        symbolTable.put(id, currentAddress);
        currentAddress += variableSize;
        generatedCode.genera(Macchina.PUSHIMM, 0);
    }

    public boolean contains(String identifier) {
        return this.symbolTable.contains(identifier);
    }

    public Integer getLocation(String identifier) {
        return this.symbolTable.get(identifier);
    }
}
