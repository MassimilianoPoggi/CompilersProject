package edu.di.unimi.it.compilers.project.utils;

import java.util.*;

public class SymbolTable<T> {
    private final Optional<SymbolTable<T>> parent;
    private final Map<String, T> table;

    public SymbolTable() {
        this(Optional.empty());
    }

    private SymbolTable(Optional<SymbolTable<T>> parent) {
        this.parent = parent;
        this.table = new HashMap<>();
    }

    public Set<String> getDeclaredSymbols() {
        Set<String> result = new HashSet<>(table.keySet());
        if (parent.isPresent())
            result.addAll(parent.get().getDeclaredSymbols());
        return result;
    }

    public void put(String identifier, T value) {
        table.put(identifier, value);
    }

    public T get(String identifier) {
        if (table.containsKey(identifier))
            return table.get(identifier);

        return parent.get().get(identifier);
    }

    public boolean contains(String identifier) {
        return table.containsKey(identifier) ||
                parent.isPresent() && parent.get().contains(identifier);
    }

    public SymbolTable<T> pushScope() {
        return new SymbolTable<>(Optional.of(this));
    }

    public SymbolTable<T> popScope() {
        return parent.get();
    }

    public SymbolTable<T> intersectInnerScope(SymbolTable<T> other) {
        SymbolTable<T> result = this.parent.get();
        Set<String> commonIdentifiers = new HashSet<>(this.table.keySet());
        commonIdentifiers.retainAll(other.table.keySet());

        for (String identifier : commonIdentifiers) {
            T value = this.get(identifier);
            if (Objects.equals(value, other.get(identifier)))
                result.put(identifier, value);
        }

        return result;
    }
}
