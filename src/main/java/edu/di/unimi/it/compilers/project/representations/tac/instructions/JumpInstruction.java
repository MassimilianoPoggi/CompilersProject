package edu.di.unimi.it.compilers.project.representations.tac.instructions;

import edu.di.unimi.it.compilers.project.representations.tac.ThreeAddressCodeVisitor;

import java.util.Objects;

public class JumpInstruction implements ThreeAddressInstruction {
    private int address;

    public JumpInstruction() {
        this(-1);
    }

    public JumpInstruction(int address) {
        this.address = address;
    }

    public void setJumpAddress(int address) {
        this.address = address;
    }

    public int getAddress() {
        return address;
    }

    @Override
    public <T> T accept(ThreeAddressCodeVisitor<T> visitor) {
        return visitor.visitJump(this);
    }

    @Override
    public String toString() {
        return "jump " + address;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        JumpInstruction that = (JumpInstruction) o;
        return address == that.address;
    }

    @Override
    public int hashCode() {
        return Objects.hash(address);
    }
}
