package edu.di.unimi.it.compilers.project.reporting;

import edu.di.unimi.it.compilers.project.parsing.LanguageParserSym;
import java_cup.runtime.ComplexSymbolFactory;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;
import java.util.List;
import java.util.logging.Logger;
import java.util.stream.Collectors;

public class ErrorReporter {
    public String getSyntaxErrorMessage(ComplexSymbolFactory.ComplexSymbol info, List<Integer> expectedTokens) {
        StringBuilder builder = new StringBuilder();
        addErrorLocation(builder, info.getLeft());
        builder.append(": ");
        addSyntaxErrorDescription(builder, info, expectedTokens);
        builder.append("\n");
        showErrorVisually(builder, info.getLeft());
        return builder.toString();
    }

    public String getWarningMessage(String message, ComplexSymbolFactory.Location location) {
        StringBuilder builder = new StringBuilder();
        addErrorLocation(builder, location);
        builder.append(": warning: ");
        builder.append(message);
        builder.append("\n");
        showErrorVisually(builder, location);
        return builder.toString();
    }

    private void addSyntaxErrorDescription(StringBuilder builder, ComplexSymbolFactory.ComplexSymbol info,
                                           List<Integer> expectedTokens) {
        builder.append("error: ");
        if (info.sym == LanguageParserSym.error)
            builder.append(info.value);
        else {
            builder.append("unexpected token ");
            if (info.value == null)
                builder.append("'" + info.getName() + "'");
            else
                builder.append(info.getName() + " with value '" + info.value + "'");
            if (!expectedTokens.isEmpty()) {
                builder.append("\n\texpected tokens: ");
                builder.append(
                        expectedTokens
                                .stream()
                                .map(this::tokenAsString)
                                .collect(Collectors.joining(", ")));
            }
        }
    }

    private void addErrorLocation(StringBuilder builder, ComplexSymbolFactory.Location location) {
        builder.append(location.getUnit());
        builder.append(":");
        builder.append(location.getLine());
        builder.append(":");
        builder.append(location.getColumn());
    }

    private void showErrorVisually(StringBuilder builder, ComplexSymbolFactory.Location location) {
        File file = new File(location.getUnit());
        if (!file.canRead()) {
            builder.append("\tcannot read input file to show error");
            return;
        }

        builder.append("\t");
        builder.append(getFileLine(file, location.getLine() - 1));
        builder.append("\n\t");
        for (int i = 0; i < location.getColumn() - 1; i++)
            builder.append(" ");
        builder.append("^");
    }

    private String getFileLine(File file, int line) {
        try (FileReader fileReader = new FileReader(file)) {
            LineNumberReader reader = new LineNumberReader(fileReader);
            while (reader.getLineNumber() < line)
                reader.readLine();

            return reader.readLine();
        } catch (IOException e) {
            Logger.getLogger(this.getClass().getName())
                    .severe("Error while reading/closing file to display error context: " + e.getMessage());
        }
        return "";
    }

    private String tokenAsString(int tokenId) {
        switch (tokenId) {
            case LanguageParserSym.TIMES:
                return "'*'";
            case LanguageParserSym.MOD:
                return "'%'";
            case LanguageParserSym.INPUT:
                return "'input'";
            case LanguageParserSym.CR:
                return "'\\n'";
            case LanguageParserSym.QUESTION:
                return "'?'";
            case LanguageParserSym.PLUS:
                return "'+'";
            case LanguageParserSym.RPAREN:
                return "')'";
            case LanguageParserSym.COLON:
                return "':'";
            case LanguageParserSym.LPAREN:
                return "'('";
            case LanguageParserSym.ID:
                return "identifier";
            case LanguageParserSym.STRING:
                return "string";
            case LanguageParserSym.EQUALS:
                return "'='";
            case LanguageParserSym.NUMBER:
                return "number";
            case LanguageParserSym.EOF:
                return "EOF";
            case LanguageParserSym.DIVIDE:
                return "'/'";
            case LanguageParserSym.NEWLINE:
                return "'\\n'";
            case LanguageParserSym.BEGIN_LOOP:
                return "'loop'";
            case LanguageParserSym.MINUS:
                return "'-'";
            case LanguageParserSym.END_LOOP:
                return "'endLoop'";
            case LanguageParserSym.OUTPUT:
                return "'output'";
            default:
                throw new IllegalArgumentException("Token ID out of range");
        }
    }
}
